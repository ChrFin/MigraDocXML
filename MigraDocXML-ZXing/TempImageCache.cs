﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigraDocXML_ZXing
{
    [Obsolete("This class is no longer required since MigraDoc v1.50 allows for images to be added straight from memory as base 64 strings")]
    public static class TempImageCache
    {

        private static Dictionary<Document, List<string>> _documentImageFiles = new Dictionary<Document, List<string>>();

        private static List<string> _previouslyFailedDeletions = new List<string>();


        internal static void AddTempFile(Document document, string filePath)
        {
            if (!_documentImageFiles.ContainsKey(document))
            {
                _documentImageFiles[document] = new List<string>();
                document.Discarded += OnDocumentDiscarded;
            }
            _documentImageFiles[document].Add(filePath);
        }


        private static void OnDocumentDiscarded(object sender, EventArgs e)
        {
            ClearTempFiles(sender as Document);
        }


        public static void ClearTempFiles(Document document)
        {
            if (_documentImageFiles.ContainsKey(document))
            {
                foreach (var tempFile in _documentImageFiles[document])
                {
                    try
                    {
                        System.IO.File.Delete(tempFile);
                    }
                    catch
                    {
                        _previouslyFailedDeletions.Add(tempFile);
                    }
                }
                _documentImageFiles.Remove(document);
                document.Discarded -= OnDocumentDiscarded;
            }
            
            for(int i = _previouslyFailedDeletions.Count - 1; i >= 0; i--)
            {
                string tempFile = _previouslyFailedDeletions[i];

                try
                {
                    if (System.IO.File.Exists(tempFile))
                        System.IO.File.Delete(tempFile);
                    _previouslyFailedDeletions.RemoveAt(i);
                }
                catch { }
            }
        }

    }
}
