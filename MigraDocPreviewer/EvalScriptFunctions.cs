﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigraDocPreviewer
{
	public class EvalScriptFunctions
	{
		public static int[] GetPrimeFactors(object[] args)
		{
			if (args.Length != 1 || !(args[0] is int))
				throw new Exception("GetPrimeFactors expects only one argument of type int");
			var output = new List<int>();
			int a = (int)args[0];
			int b = 2;
			while(a >= b)
			{
				if (a % b == 0)
				{
					output.Add(b);
					a /= b;
				}
				else
					b++;
			}
			return output.ToArray();
		}
	}
}
