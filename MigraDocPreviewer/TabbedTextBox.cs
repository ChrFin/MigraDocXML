﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;

namespace MigraDocPreviewer
{
    public class TabbedTextBox : TextBox
    {

        private int _tabSize = 4;
        public int TabSize
        {
            get => _tabSize;
            set
            {
                if (value != _tabSize)
                {
                    if (value < 0)
                        throw new Exception("Invalid tab size, cannot be negative");
                    _tabSize = value;
                }
            }
        }

        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            Text = Text.Replace("\t", new string(' ', TabSize));
            base.OnTextChanged(e);
        }

        /// <summary>
        /// Returns which line the caret is on
        /// </summary>
        public int CaretLine
        {
            get
            {
                int returnCount = 0;
                int newLineIndex = Text.IndexOf(Environment.NewLine);
                while (newLineIndex >= 0)
                {
                    returnCount++;
                    newLineIndex = Text.IndexOf(Environment.NewLine, newLineIndex + 1);
                }
                return returnCount;
            }
        }

        /// <summary>
        /// Returns the caret's index within its line
        /// </summary>
        public int CaretLineIndex
        {
            get
            {
                int newLineIndex = Text.LastIndexOf(Environment.NewLine, CaretIndex);
                newLineIndex = (newLineIndex < 0) ? 0 : newLineIndex + Environment.NewLine.Length;

                return CaretIndex - newLineIndex;
            }
        }

        /// <summary>
        /// Returns the text of the line that the caret is on
        /// </summary>
        public string CaretLineText
        {
            get
            {
                int prevLineIndex = Text.LastIndexOf(Environment.NewLine, CaretIndex);
                prevLineIndex = (prevLineIndex < 0) ? 0 : prevLineIndex + Environment.NewLine.Length;

                int nextLineIndex = Text.IndexOf(Environment.NewLine, CaretIndex);
                if (nextLineIndex < 0)
                    return Text.Substring(prevLineIndex);
                return Text.Substring(prevLineIndex, nextLineIndex - prevLineIndex);
            }
        }


        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                int caretIndex = CaretIndex;

                if (SelectionLength > 0)
                {
                    caretIndex = SelectionStart;
                    Text = Text.Remove(SelectionStart, SelectionLength);
                    CaretIndex = caretIndex;
                }

                int lineIndex = CaretLineIndex;
                int lineIndexMod = lineIndex % TabSize;
                string lineText = CaretLineText;

                //If TabSize is 4 and Tab key is pressed at index 1 on the line, insert 3 spaces to move to the next 'tab point'
                int spaceCount = TabSize;
                if (string.IsNullOrWhiteSpace(lineText.Substring(0, lineIndex)))
                {
                    spaceCount = TabSize;
                }
                else
                    spaceCount = TabSize - (lineIndex % TabSize);

                Text = Text.Insert(caretIndex, new string(' ', spaceCount));
                CaretIndex = caretIndex + spaceCount;
                e.Handled = true;
            }
            else if (e.Key == Key.Enter)
            {
                int caretIndex = CaretIndex;

                string lineText = CaretLineText;
                int whiteStartLength = lineText.Length - lineText.TrimStart().Length;
                string insertText = Environment.NewLine + new string(' ', whiteStartLength);
                Text = Text.Insert(caretIndex, insertText);
                CaretIndex = caretIndex + insertText.Length;
                e.Handled = true;
            }

            base.OnPreviewKeyDown(e);
        }

    }
}
