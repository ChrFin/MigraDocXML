﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace MigraDocPreviewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindowVM ViewModel { get; private set; }

        public MainWindow()
        {
            InitializeComponent();

            ViewModel = new MainWindowVM();

            Action previewFileChangedAction = delegate () { Browser.Navigate(ViewModel.PreviewFile); };
            ViewModel.PreviewFileChanged += (sender, args) => Dispatcher.Invoke(DispatcherPriority.Normal, previewFileChangedAction);

            Action previewFileUpdateStartingAction = delegate () { Browser.Navigate((Uri)null); };
            ViewModel.PreviewFileUpdateStarting += (sender, args) => Dispatcher.Invoke(DispatcherPriority.Normal, previewFileUpdateStartingAction);

            Action previewFileUpdateFinishedAction = delegate () { Browser.Navigate(ViewModel.PreviewFile); };
            ViewModel.PreviewFileUpdateFinished += (sender, args) => Dispatcher.Invoke(DispatcherPriority.Normal, previewFileUpdateFinishedAction);

            Action<ErrorEventArgs> errorOccurredAction = delegate (ErrorEventArgs args) { MessageBox.Show(args.GetException().Message); };
            ViewModel.ErrorOccurred += (sender, args) => Dispatcher.Invoke(DispatcherPriority.Normal, errorOccurredAction, args);

            ViewModel.LayoutFile = Properties.Settings.Default.LayoutFile;
            ViewModel.DataFile = Properties.Settings.Default.DataFile;
            ViewModel.PreviewFile = Properties.Settings.Default.PreviewFile;
            
            DataContext = ViewModel;
        }


        private void SettingsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var settingsWindow = new SettingsWindow(SettingsWindowVM.LoadFromSettings());
            if (settingsWindow.ShowDialog() == true)
            {
                settingsWindow.ViewModel.SaveToSettings();
                ViewModel.LayoutFile = settingsWindow.ViewModel.LayoutFile;
                ViewModel.DataFile = settingsWindow.ViewModel.DataFile;
                ViewModel.PreviewFile = settingsWindow.ViewModel.PreviewFile;
            }
        }
    }



    public class MainWindowVM : INotifyPropertyChanged
    {
        public event ErrorEventHandler ErrorOccurred;
        
        public GenericCommand<MainWindowVM> SaveLayoutCommand { get; private set; }

        public GenericCommand<MainWindowVM> SaveDataCommand { get; private set; }
        
        public MainWindowVM()
        {
            SaveLayoutCommand = new GenericCommand<MainWindowVM>(
                x => File.WriteAllText(x.LayoutFile, x.LayoutText)
            );

            SaveDataCommand = new GenericCommand<MainWindowVM>(
                x => File.WriteAllText(x.DataFile, x.DataText)
            );

            MigraDocXML_ZXing.Setup.Run();
        }



        private FileSystemWatcher _layoutFileWatcher;

        private string _layoutFile;
        public string LayoutFile
        {
            get => _layoutFile;
            set
            {
                if (_layoutFile == value)
                    return;

                _layoutFile = value;

                //Try to read the contents of the new file and set up a file watcher on it
                //If it fails, set LayoutText to empty string and turn off file watcher
                try
                {
                    LayoutText = File.ReadAllText(_layoutFile);

                    if (_layoutFileWatcher != null)
                        _layoutFileWatcher.EnableRaisingEvents = false;

                    _layoutFileWatcher = new FileSystemWatcher(System.IO.Path.GetDirectoryName(LayoutFile), System.IO.Path.GetFileName(_layoutFile));
                    _layoutFileWatcher.Created += OnLayoutFileChanged;
                    _layoutFileWatcher.Changed += OnLayoutFileChanged;
                    _layoutFileWatcher.Renamed += OnLayoutFileChanged;
                    _layoutFileWatcher.EnableRaisingEvents = true;
                }
                catch
                {
                    LayoutText = "";
                    if (_layoutFileWatcher != null)
                        _layoutFileWatcher.EnableRaisingEvents = false;
                    if (!string.IsNullOrEmpty(_layoutFile))
                        ErrorOccurred?.Invoke(this, new ErrorEventArgs(new Exception("Failed to load layout file")));
                }
            }
        }

        private void OnLayoutFileChanged(object sender, EventArgs e)
        {
            try
            {
                LayoutText = File.ReadAllText(LayoutFile);
                LayoutTextDirty = false;
                UpdatePreview();
            }
            catch
            {
                LayoutTextDirty = true;
            }
        }
        
        private string _layoutText;
        public string LayoutText
        {
            get => _layoutText;
            set
            {
                if(value != _layoutText)
                {
                    _layoutText = value;
                    DispatchPropertyChanged(nameof(LayoutText));
                    LayoutTextDirty = true;
                }
            }
        }

        private bool _layoutTextDirty;
        public bool LayoutTextDirty
        {
            get => _layoutTextDirty;
            set
            {
                if(value != _layoutTextDirty)
                {
                    _layoutTextDirty = value;
                    DispatchPropertyChanged(nameof(LayoutTextDirty));
                }
            }
        }



        private FileSystemWatcher _dataFileWatcher;

        private string _dataFile;
        public string DataFile
        {
            get => _dataFile;
            set
            {
                if (_dataFile == value)
                    return;

                _dataFile = value;

                //Try to read the contents of the new file and set up a file watcher on it
                //If it fails, set DataText to empty string and turn off file watcher
                try
                {
                    DataText = File.ReadAllText(_dataFile);

                    if (_dataFileWatcher != null)
                        _dataFileWatcher.EnableRaisingEvents = false;

                    _dataFileWatcher = new FileSystemWatcher(System.IO.Path.GetDirectoryName(_dataFile), System.IO.Path.GetFileName(_dataFile));
                    _dataFileWatcher.Created += OnDataFileChanged;
                    _dataFileWatcher.Changed += OnDataFileChanged;
                    _dataFileWatcher.Renamed += OnDataFileChanged;
                    _dataFileWatcher.EnableRaisingEvents = true;
                }
                catch
                {
                    DataText = "";
                    if (_dataFileWatcher != null)
                        _dataFileWatcher.EnableRaisingEvents = false;
                    if (!string.IsNullOrEmpty(_dataFile))
                        ErrorOccurred?.Invoke(this, new ErrorEventArgs(new Exception("Failed to load data file")));
                }
            }
        }

        private void OnDataFileChanged(object sender, EventArgs e)
        {
            try
            {
                DataText = File.ReadAllText(DataFile);
                DataTextDirty = false;
                UpdatePreview();
            }
            catch
            {
                DataTextDirty = true;
            }
        }
        
        private string _dataText;
        public string DataText
        {
            get => _dataText;
            set
            {
                if(value != _dataText)
                {
                    _dataText = value;
                    DispatchPropertyChanged(nameof(DataText));
                    DataTextDirty = true;
                }
            }
        }

        private bool _dataTextDirty;
        public bool DataTextDirty
        {
            get => _dataTextDirty;
            set
            {
                if(value != _dataTextDirty)
                {
                    _dataTextDirty = value;
                    DispatchPropertyChanged(nameof(DataTextDirty));
                }
            }
        }



        private FileSystemWatcher _previewFileWatcher;

        public event EventHandler PreviewFileChanged;

        public event EventHandler PreviewFileUpdateStarting;

        public event EventHandler PreviewFileUpdateFinished;

        private string _previewFile;

        public string PreviewFile
        {
            get => _previewFile;
            set
            {
                if (_previewFile == value)
                    return;

                _previewFile = value;

                try
                {
                    if (_previewFileWatcher != null)
                        _previewFileWatcher.EnableRaisingEvents = false;
                    _previewFileWatcher = new FileSystemWatcher(System.IO.Path.GetDirectoryName(_previewFile), System.IO.Path.GetFileName(_previewFile));
                    _previewFileWatcher.Changed += (sender, args) => PreviewFileChanged?.Invoke(this, null);

                    PreviewFileChanged?.Invoke(this, null);
                }
                catch
                {
                    if (_previewFileWatcher != null)
                        _previewFileWatcher.EnableRaisingEvents = false;
                    if (!string.IsNullOrEmpty(_previewFile))
                        ErrorOccurred?.Invoke(this, new ErrorEventArgs(new Exception("Failed to load preview file")));
                }
            }
        }

        private bool _updatingPreview = false;

        private void UpdatePreview()
        {
            if (_updatingPreview)
                return;
            _updatingPreview = true;

            int retryCount = 20;
            PdfSharp.Pdf.PdfDocument renderedDoc = null;

            try
            {
                PreviewFileUpdateStarting?.Invoke(this, null);

                var reader = new PdfXmlReader(LayoutFile);

				if (string.IsNullOrWhiteSpace(DataFile))
					renderedDoc = reader.Render();
				else if (DataFile.ToUpper().EndsWith(".CSV"))
					renderedDoc = reader.RenderWithCsvFile(DataFile);
				else if (DataFile.ToUpper().EndsWith(".JSON"))
					renderedDoc = reader.RenderWithJsonFile(DataFile);
				else if (DataFile.ToUpper().EndsWith(".XML"))
					renderedDoc = reader.RenderWithXmlFile(DataFile);
				else
					throw new Exception("Unrecognised data file format");

                string outputFile = PreviewFile;

                for (int i = 1; i <= retryCount; i++)
                {
                    try
                    {
						renderedDoc.Save(outputFile);
                        break;
                    }
                    catch (IOException ex)
                    {
                        if (i < retryCount)
                            System.Threading.Thread.Sleep(50);
                        else
                            ErrorOccurred?.Invoke(this, new ErrorEventArgs(ex));
                    }
                    catch (Exception ex)
                    {
                        ErrorOccurred?.Invoke(this, new ErrorEventArgs(ex));
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorOccurred?.Invoke(this, new ErrorEventArgs(ex));
            }
            finally
            {
                _updatingPreview = false;
            }

            PreviewFileUpdateFinished?.Invoke(this, null);
        }



        public event PropertyChangedEventHandler PropertyChanged;
        private void DispatchPropertyChanged(string name) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
    }
}
