# MigraDocXML Lessons

### Lesson 1.0 : Setup
[PDF](Lessons/Lesson 1.0 - Setup.pdf) - [XML](Lessons/Lesson 1.0 - Setup.xml)

## Display Object Models

### Lesson 2.0 - Sections & Page Setup
[PDF](Lessons/Lesson 2.0 - Sections & Page Setup.pdf) - [XML](Lessons/Lesson 2.0 - Sections & Page Setup.xml)

### Lesson 2.1 - Paragraphs
[PDF](Lessons/Lesson 2.1 - Paragraphs.pdf) - [XML](Lessons/Lesson 2.1 - Paragraphs.xml)

### Lesson 2.2 - Tables
[PDF](Lessons/Lesson 2.2 - Tables.pdf) - [XML](Lessons/Lesson 2.2 - Tables.xml)

### Lesson 2.3 - Images
[PDF](Lessons/Lesson 2.3 - Images.pdf) - [XML](Lessons/Lesson 2.3 - Images.xml)

### Lesson 2.4 - TextFrames
[PDF](Lessons/Lesson 2.4 - TextFrames.pdf) - [XML](Lessons/Lesson 2.4 - TextFrames.xml)

### Lesson 2.5 - PointLists
[PDF](Lessons/Lesson 2.5 - PointLists.pdf) - [XML](Lessons/Lesson 2.5 - PointLists.xml)

### Lesson 2.6 - Headers & Footers
[PDF](Lessons/Lesson 2.6 - Headers & Footers.pdf) - [XML](Lessons/Lesson 2.6 - Headers & Footers.xml)

### Lesson 2.7 - Hyperlinks & Bookmarks
[PDF](Lessons/Lesson 2.7 - Hyperlinks & Bookmarks.pdf) - [XML](Lessons/Lesson 2.7 - Hyperlinks & Bookmarks.xml)

## Scripts

### Lesson 3.0 - Introduction To Scripts
[PDF](Lessons/Lesson 3.0 - Introduction To Scripts.pdf) - [XML](Lessons/Lesson 3.0 - Introduction To Scripts.xml)

### Lesson 3.1 - Conditionals
[PDF](Lessons/Lesson 3.1 - Conditionals.pdf) - [XML](Lessons/Lesson 3.1 - Conditionals.xml)

### Lesson 3.2 - Var & Set
[PDF](Lessons/Lesson 3.2 - Var & Set.pdf) - [XML](Lessons/Lesson 3.2 - Var & Set.xml)

### Lesson 3.3 - Arrays, Dictionaries & More Operators
[PDF](Lessons/Lesson 3.3 - Arrays, Dictionaries & More Operators.pdf) - [XML](Lessons/Lesson 3.3 - Arrays, Dictionaries & More Operators.xml)

### Lesson 3.4 - Loops & Jumps
[PDF](Lessons/Lesson 3.4 - Loops & Jumps.pdf) - [XML](Lessons/Lesson 3.4 - Loops & Jumps.xml)

### Lesson 3.5 - Injecting CSV Data
[PDF](Lessons/Lesson 3.5 - Injecting CSV Data.pdf) - [XML](Lessons/Lesson 3.5 - Injecting CSV Data.xml) - [CSV](Lessons/Lesson 3.5 - Injecting CSV Data.csv)

### Lesson 3.6 - Injecting JSON Data
[PDF](Lessons/Lesson 3.6 - Injecting JSON Data.pdf) - [XML](Lessons/Lesson 3.6 - Injecting JSON Data.xml) - [JSON](Lessons/Lesson 3.6 - Injecting JSON Data.json)

### Lesson 3.7 - Injecting .NET Data
[PDF](Lessons/Lesson 3.7 - Injecting .NET Data.pdf) - [XML](Lessons/Lesson 3.7 - Injecting .NET Data.xml) - [C#](Lessons/Lesson 3.7 - Injecting .NET Data.cs)

### Lesson 3.8 - Injecting XML Data
[PDF](Lessons/Lesson 3.8 - Injecting XML Data.pdf) - [XML](Lessons/Lesson 3.8 - Injecting XML Data.xml) - [XML DATA](Lessons/Lesson 3.8 - Injecting XML Data.xml)

### Lesson 3.9 - Accessing The DOM
[PDF](Lessons/Lesson 3.9 - Accessing The DOM.pdf) - [XML](Lessons/Lesson 3.9 - Accessing The DOM.xml)

### Lesson 3.10 - Lambda Functions
[PDF](Lessons/Lesson 3.10 - Lambda Functions.pdf) - [XML](Lessons/Lesson 3.10 - Lambda Functions.xml)

### Lesson 3.11 - String Functions (Reference)
[PDF](Lessons/Lesson 3.11 - String Functions.pdf) - [XML](Lessons/Lesson 3.11 - String Functions.xml)

### Lesson 3.12 - Numeric Functions (Reference)
[PDF](Lessons/Lesson 3.12 - Numeric Functions.pdf) - [XML](Lessons/Lesson 3.12 - Numeric Functions.xml)

### Lesson 3.13 - DateTime Functions (Reference)
[PDF](Lessons/Lesson 3.13 - DateTime Functions.pdf) - [XML](Lessons/Lesson 3.13 - DateTime Functions.xml)

### Lesson 3.14 - Enumerable Functions (Reference)
[PDF](Lessons/Lesson 3.14 - Enumerable Functions.pdf) - [XML](Lessons/Lesson 3.14 - Enumerable Functions.xml)

### Lesson 3.15 - Misc Functions (Reference)
[PDF](Lessons/Lesson 3.15 - Misc Functions.pdf) - [XML](Lessons/Lesson 3.15 - Misc Functions.xml)

### Lesson 3.16 - Custom Functions
[PDF](Lessons/Lesson 3.16 - Custom Functions.pdf) - [XML](Lessons/Lesson 3.16 - Custom Functions.xml) - [C#](Lessons/Lesson 3.16 - Custom Functions.cs)

## Styles

### Lesson 4.0 - Introduction To Styles
[PDF](Lessons/Lesson 4.0 - Introduction To Styles.pdf) - [XML](Lessons/Lesson 4.0 - Introduction To Styles.xml)

### Lesson 4.1 - Style Scope & Precedence
[PDF](Lessons/Lesson 4.1 - Style Scope & Precedence.pdf) - [XML](Lessons/Lesson 4.1 - Style Scope & Precedence.xml)

### Lesson 4.2 - Advanced Style Targeting
[PDF](Lessons/Lesson 4.2 - Advanced Style Targeting.pdf) - [XML](Lessons/Lesson 4.2 - Advanced Style Targeting.xml)

## Further Topics

### Lesson 5.0 - Resources
[PDF](Lessons/Lesson 5.0 - Resources.pdf) - [XML](Lessons/Lesson 5.0 - Resources.xml) - [External](Lessons/Lesson 5.0 - Resources - External.xml)

### Lesson 5.1 - Barcodes With ZXing.NET
[PDF](Lessons/Lesson 5.1 - Barcodes With ZXing.NET.pdf) - [XML](Lessons/Lesson 5.1 - Barcodes With ZXing.NET.xml)

### Lesson 5.2 - Charts
[PDF](Lessons/Lesson 5.2 - Charts.pdf) - [XML](Lessons/Lesson 5.2 - Charts.xml)

### Lesson 5.3 - Graphics
[PDF](Lessons/Lesson 5.3 - Graphics.pdf) - [XML](Lessons/Lesson 5.3 - Graphics.xml)

### Lesson 5.4 - Combining Documents
[PDF](Lessons/Lesson 5.4 - Combining Documents.pdf) - [XML](Lessons/Lesson 5.4 - Combining Documents.xml)

### Lesson 5.5 - Annotations
[PDF](Lessons/Lesson 5.5 - Annotations.pdf) - [XML](Lessons/Lesson 5.5 - Annotations.xml)
