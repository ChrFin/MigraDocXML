# MigraDocXML

[Lessons available here](Lessons.md)

## Introduction
From having to do a fair amount of work producing PDFs, I found MigraDoc is currently the only really viable open source .NET option available. It includes some excellent features that give a lot of control over creating PDFs, while still staying high level enough to be easy to understand.

However, the way document designs are written does also present some major drawbacks:

- Languages like C#/VB.NET just aren't well suited to the hierarchical nature of building up a display object model, and creating anything beyond a simple layout can quickly become a real challenge to remain readable. Other XML based languages such as HTML & XAML have proven that XML is far better suited to this sort of work.
- Document designs are stuck within your project assembly. This prevents quick design changes without recompiling your code, also making it much harder to have a product that ships to multiple customers, where each customer has their own requirements for PDF exports.
- Your programmers shouldn't be creating PDF reports. As a programmer, writing PDF designs is just plain boring, most of what you're writing is very boilerplate stuff, often plugging in slightly different numbers and recompiling to see if things finally line up right. MigraDocXML has been designed to be usable by people who don't come from a programming background but are willing to learn a few basic concepts.

MigraDocXML aims to solve these problems, while still keeping as much flexibility as possible. Below are some of its features:

- Supports inserting data from JSON, CSV or XML files, as well as directly from .NET objects
- MigraDoc's styling has been completely redesigned, with MigraDocXML offering a full cascading style system
- Designs can define their own variables, conditional and iterative logic. It also includes EvalScript, a lightweight expression evaluator which provides a lot of advanced language features. These combine to allow the easy writing of flexible designs that can adapt to your data
- Resources (stored either internally or externally) support code reuse
- Bridges the gap between MigraDoc & PDFSharp, allowing for the addition of custom drawings to your designs
- Includes an extension library that makes use of ZXing.NET to support inserting barcodes

## Example
Below is a demonstration of a layout file which takes in a json file to produce [this](Samples/sample.pdf).

json:
```
{
    "Name": "MigraDocXML",
    "Positives": [
        "Simple to learn",
        "Easily readable design layouts",
        "Full cascading style system",
        "Support for PDFSharp graphics",
        "Includes a powerful lightweight expression evaluator",
        "Regular updates"
    ],
    "Negatives": [
    ]
}
```

xml:
```
<?xml version="1.0"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="p">
        <Setters Format.Font.Name="Calibri" Format.SpaceBefore="2mm" Format.SpaceAfter="2mm"/>
    </Style>

    <Style Target="p" Name="Heading">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Section>
        <p Style="Heading" Format.Font.Size="15" Format.Alignment="Center">Reasons you should use {Model.Name}</p>

        <p Style="Heading">Positives:</p>
        <ForEach Var="positive" In="Model.Positives.OrderByDesc(x => x.Length)">
            <p>{positive}</p>
        </ForEach>

        <p Style="Heading">Negatives:</p>
        <ForEach Var="negative" In="Model.Negatives">
            <p>{negative}</p>
        </ForEach>
    </Section>
</Document>
```

## Editor

MigraDocXML also includes a small WPF app for easily previewing the effect of changes to the layout or data on your output PDF. You can use the incredibly basic text editors included in the app, or, use whichever one you normally do. Either way, each time you save changes to your documents, the PDF will automatically get regenerated.

![alt text](Samples/Preview_app.jpg)

## Sponsors

Part of the development of MigraDocXML is kindly sponsored by:

![Omicron Lab](Samples/omicronlab.png)