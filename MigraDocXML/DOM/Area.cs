﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MigraDocXML.DOM
{
	public class Area : LogicalElement
	{
		private PdfSharp.Drawing.XRect _xRect;
		public PdfSharp.Drawing.XRect GetXRect() => _xRect;

		public Area()
		{
			IsLogical = false;
		}

		public override void Run(Action childProcessor)
		{
			_xRect = new PdfSharp.Drawing.XRect(
				X?.Points ?? 0,
				Y?.Points ?? 0,
				Width?.Points ?? 0,
				Height?.Points ?? 0
			);

			childProcessor();
		}

		public Unit X { get; set; }

		public Unit Y { get; set; }

		public Unit Width { get; set; }

		public Unit Height { get; set; }
	}
}
