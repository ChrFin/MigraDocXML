﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MigraDocXML.DOM
{
	public class Output : LogicalElement
	{
		public Dictionary<string, PdfSharp.Pdf.PdfDocument> Sources { get; private set; }

		public Dictionary<string, PdfSharp.Drawing.XPdfForm> PdfForms { get; private set; }

		public override void Run(Action childProcessor)
		{
			var documents = GetParentOfType<Documents>();

			var docChildren = documents.GetAllDescendents().OfType<Document>();
			if (docChildren.Any(x => string.IsNullOrWhiteSpace(x.Name)))
				throw new Exception("Please ensure all documents are given names");
			docChildren = docChildren.Where(x => !string.IsNullOrWhiteSpace(x.Name));

			Sources = docChildren
				.ToDictionary(x => x.Name, x => x.AddGraphics(documents.Unicode));

			PdfForms = docChildren.ToDictionary(
				x => x.Name, 
				x => {
					using (var stream = new MemoryStream())
					{
						x.GetRenderer().Save(stream, false);
						return PdfSharp.Drawing.XPdfForm.FromStream(stream);
					}
				}
			);

			foreach (var source in Sources)
				NewVariable(source.Key, source.Value);

			childProcessor();
		}
	}
}
