﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace MigraDocXML.DOM
{
	public class Documents : LogicalElement
	{
		/// <summary>
		/// Signifies that the document is currently being discarded by the build process
		/// </summary>
		public event EventHandler Discarded;

		internal void DispatchDiscarded() => Discarded?.Invoke(this, null);

		public EvalScript.Runner ScriptRunner { get; private set; }

		private XmlElement _xmlElement;
		public XmlElement GetXmlElement() => _xmlElement;
		internal void SetXmlElement(XmlElement xmlElement) => _xmlElement = xmlElement;

		private PdfSharp.Pdf.PdfDocument _result = new PdfSharp.Pdf.PdfDocument();
		//What gets modified as the final output by the Output element
		public PdfSharp.Pdf.PdfDocument GetResult() => _result;
		
		public string ImagePath { get; set; }

		public string ResourcePath { get; set; }

		public Documents(object modelData, EvalScript.Runner scriptRunner)
		{
			ScriptRunner = scriptRunner ?? throw new ArgumentNullException(nameof(scriptRunner));
			NewVariable("Model", modelData);
		}

		public override Document GetDocument()
		{
			return Children.OfType<Document>().FirstOrDefault();
		}

		public override void SetUnknownAttribute(string name, object value)
		{
			//Ignore any unrecognised attributes, as these are most likely for referencing namespace
		}

		public override void Run(Action childProcessor)
		{
			childProcessor();
		}

		public bool Unicode { get; set; }
	}
}
