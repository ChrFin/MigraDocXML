<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Section PageSetup.PageFormat="A5" PageSetup.Orientation="Landscape">
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 2.0 - Sections &amp; Page Setup</p>

        <p>Before we look at creating a document with some simple content, it is first worth quickly looking at document sections. When we add content to our PDF document, we don't actually add it directly to the document, instead we add a section to the document, then add our content to the section. The section defines how pages within it are set up, for example, page size, margins, orientation &amp; header/footer configurations. This allows for one document to contain pages of varying sizes within it, should you wish.</p>

        <p>By default, each section is set to use A4 size pages in portrait mode with margins of ~1 inch on each side.</p>

        <p>Here's an example to show how this section was set up:</p>

        <p Style="Code">
            &lt;Document xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot;
            {Space(10)}xsi:noNamespaceSchemaLocation=&quot;C:\My schema location.xsd&quot;&gt;
            {Space(4)}&lt;Section PageSetup.PageFormat=&quot;A5&quot; PageSetup.Orientation=&quot;Landscape&quot;&gt;
            
            {Space(4)}&lt;/Section&gt;
            &lt;/Document&gt;
            
        </p>

        <p>Scroll down for more examples.</p>
    </Section>

    <Section PageSetup.PageFormat="A6" PageSetup.Orientation="Portrait" PageSetup.HorizontalMargin="2mm" PageSetup.TopMargin="1cm">
        <p>This is an example of a small page with tiny horizontal margins set to make the most of what little space we have</p>

        <p Style="Code">
            &lt;Section PageSetup.PageFormat=&quot;A6&quot; 
            {Space(9)}PageSetup.Orientation=&quot;Portrait&quot;
            {Space(9)}PageSetup.HorizontalMargin=&quot;2mm&quot;
            {Space(9)}PageSetup.TopMargin=&quot;1cm&quot;&gt;
            
            &lt;/Section&gt;
            
        </p>

        <p Format.Font.Color="Gray">(Note, if you're viewing this lesson directly in the gitlab PDF viewer, you may not get the full effect of this section resizing, since rather than demonstrating sections at their actual size, it instead scales the size of pages so they all look the same width. As a result, the smaller sections just look like they have bigger text)</p>

        <PageBreak/>

        <p>Creating a new section automatically results in a new page being added to the new section.</p>

        <p>If you want to manually add a new page to the existing section, this can be done using the <b>&lt;PageBreak/&gt;</b> element (which has just been done to get this text appearing on its own page).</p>
    </Section>

    <Section PageSetup.PageWidth="6cm" PageSetup.PageHeight="6cm" PageSetup.Margin="0">
        <p>Here's a really REALLY tiny custom sized section with no margin on any sides</p>

        <p Style="Code">
            &lt;Section PageSetup.PageWidth=&quot;6cm&quot;
            {Space(9)}PageSetup.PageHeight=&quot;6cm&quot;
            {Space(9)}PageSetup.Margin=&quot;0&quot;&gt;
            
            &lt;/Section&gt;
            
        </p>
    </Section>

    <Section>
        <p>And finally, this is what we get if we fall back on the default settings, not setting any of the section attributes.</p>

        <p Style="Code">
            &lt;Section&gt;
            
            &lt;/Section&gt;
            
        </p>
    </Section>
</Document>