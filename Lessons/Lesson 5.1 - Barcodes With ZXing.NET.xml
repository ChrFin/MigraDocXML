<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML-ZXing/schema.xsd">

  <Style Target="Paragraph">
    <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
  </Style>

  <Style Target="Paragraph" Name="Title">
    <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
  </Style>

  <Style Target="Paragraph" Name="SubTitle">
    <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
  </Style>

  <Style Target="Paragraph" Name="Code">
    <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
  </Style>

  <Style Target="Hyperlink">
    <Setters Font.Color="Blue" Font.Underline="Single"/>
  </Style>

  <Section>
    <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 5.0 - Barcodes With ZXing.NET</p>

    <p>
      MigraDocXML also includes an extension library for integrating with the ZXing.NET library (project URL: <a>https://github.com/micjahn/ZXing.Net</a>).
    </p>

    <p>
      This uses an extended version of the MigraDocXML schema, which can be found here <a>https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML-ZXing/schema.xsd</a> which exposes a number of additional elements, chief among them being the <b>Barcode</b> element.
    </p>

    <p Style="Code" Format.SpaceAfter="3mm">&lt;Barcode Format="CODE_39" PixelHeight="50"&gt;HELLO WORLD&lt;/Barcode&gt;</p>

    <Barcode Format="CODE_39" PixelHeight="50">HELLO WORLD</Barcode>

    <p Style="SubTitle">Setup</p>

    <p>To work with barcodes in MigraDocXML you just need one extra line of code in your project:</p>

    <p Style="Code">
      <FormattedText Font.Color="Green">//At application startup</FormattedText>
      
      MigraDocXML_ZXing.Setup.Run();

      <FormattedText Font.Color="Green">//Generating documents</FormattedText>
      {NewLine()}
      <FormattedText Font.Color="Gray">
        string xmlFile = @"C:\Users\james\Documents\Test.xml";
        string pdfFile = @"C:\Users\james\Documents\Test.pdf";
        var pdfDoc = new PdfXmlReader(xmlFile).Run();
        if(pdfDoc == null)
        {Space(4)}return;
        var renderer = new MigraDoc.Rendering.PdfDocumentRenderer() {LeftBrace()} Document = pdfDoc.GetDocumentModel() {RightBrace()};
        renderer.RenderDocument();
        renderer.PdfDocument.Save(pdfFile);
      </FormattedText>
    </p>

    <p>The extra line registers all the types used by the ZXing extension with MigraDocXML.</p>


    <p Style="SubTitle">More Barcodes</p>

    <p>
      With the <b>Barcode</b> element you can specify a number of different barcode formats to use through the barcode's <b>Format</b> attribute. This gives you access to the standard attributes available for editing all barcodes. However, for more complex barcodes such as QR codes, you're better off using its own specific element, since this gives you access to options available for only that barcode type:
    </p>

    <p Style="Code" Format.SpaceAfter="2mm">&lt;QRCode ErrorCorrection="Q" LineFormat.Color="Black"&gt;Barcodes are great!&lt;/QRCode&gt;</p>

    <QRCode ErrorCorrection="Q" LineFormat.Color="Black">Barcodes are great!</QRCode>

    <p>This works, but is very small, to the point where a scanner may have trouble actually reading the QR code. This is because by default, ZXing generates the image so that each bit of data in the barcode is one pixel in size. We can try and fix this using the Width or Height attribute:</p>

    <p Style="Code" Format.SpaceAfter="2mm">&lt;QRCode ErrorCorrection="Q" LineFormat.Color="Black" Width="4cm"&gt;Barcodes are great!&lt;/QRCode&gt;</p>

    <QRCode ErrorCorrection="Q" LineFormat.Color="Black" Width="4cm">Barcodes are great!</QRCode>

    <p>
      This has technically worked, but just taking a tiny image and scaling it up to a large size has made it look really blurry. What we need is to make the actual image itself bigger, which we can control using the <b>PixelWidth</b>, <b>PixelHeight</b> &amp; <b>BarcodeMargin</b> attributes.
    </p>

    <p Style="Code" Format.SpaceAfter="2mm">
      &lt;QRCode ErrorCorrection="Q" LineFormat.Color="Black"
      {Space(8)}PixelWidth="200" PixelHeight="200" BarcodeMargin="1" Width="4cm"&gt;Barcodes are great!&lt;/QRCode&gt;
    </p>

    <QRCode ErrorCorrection="Q" LineFormat.Color="Black" PixelWidth="200" PixelHeight="200" BarcodeMargin="1" Width="4cm">Barcodes are great!</QRCode>

    <p Style="SubTitle">GS1 Barcodes</p>

    <p>When using barcodes for sending/receiving goods, often the use of GS1 barcodes is crucial. These barcodes use AI (Application Identifier) codes to describe the data, allowing for one barcode to contain multiple pieces of meaningful data at once.</p>

    <p>GS1 barcodes use a special character called the FNC1 to separate out pieces of data. In most barcode symbologies, this is the 29<super>th</super> ASCII character, though in Code128 barcodes it's the 241<super>st</super>. As an example, we can create a GS1 QR code like so:
  </p>
    
    <p Style="Code">&lt;QRCode PixelWidth="200" PixelHeight="200" BarcodeMargin="1" Width="4cm"&gt;```{Char(29)}2112345{Char(29)}17201020```&lt;/QRCode&gt;
    </p>

    <QRCode PixelWidth="200" PixelHeight="200" BarcodeMargin="1" Width="4cm">{Char(29)}2112345{Char(29)}17201020</QRCode>

    <p>In the above example we have 2 pieces of data:</p>

    <list NumberPosition="8mm">
      <p>2112345 = AI 21 with data '12345'. This means we have serial number '12345'</p>
      <p>17201020 = AI 17 with data '201020'. This gives us an expiry date of 20<super>th</super> October 2020</p>
    </list>

    <p>The full list of AIs and their meanings can be found here <a>https://www.gs1.org/sites/default/files/docs/barcodes/GS1_General_Specifications.pdf</a> on pages 150 - 155</p>

    <p>The above example is really not ideal for creating GS1 barcodes, since as you look more into the GS1 specification you'll find it's really not as simple as just separating a bunch of codes and data pieces with special characters. Plus, it's not immediately obvious where the AI code ends, and the data begins. Setting the <b>GS1Format</b> property to true, we can create a barcode to store our data far more easily, like so:</p>

    <p Style="Code">&lt;QRCode PixelWidth="200" PixelHeight="200" BarcodeMargin="1" Width="4cm" GS1Format="true"&gt;(21)12345(17)201020&lt;/QRCode&gt;
    </p>

    <QRCode PixelWidth="200" PixelHeight="200" BarcodeMargin="1" Width="2.5cm" GS1Format="true">(21)12345(17)201020</QRCode>

    <p>...and with Code128</p>

    <p Style="Code" Format.SpaceAfter="2mm">&lt;Code128 Height="1.5cm" PixelWidth="200" PixelHeight="50" GS1Format="true"&gt;(21)12345(17)201020&lt;/Code128&gt;
    </p>

    <Code128 Height="1.5cm" PixelWidth="200" PixelHeight="50" GS1Format="true">(21)12345(17)201020</Code128>
  </Section>
</Document>