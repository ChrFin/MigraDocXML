<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Style Target="FormattedText" Name="Code">
        <Setters Font.Name="Consolas" Font.Size="8" Font.Bold="false" Font.Italic="false"/>
    </Style>
    
    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 3.8 - Injecting XML Data</p>

        <p>XML data is a little more awkward than the other data types we've worked with so far. First of all, its structure does not lend itself very easily to the notation we're using for accessing data, since an XML element can have multiple children all with the same name. Additionally, everything in XML is stored as a string.</p>
        
        <p>First thing we need to do is alter our code so that on the PdfXmlReader object we call <b>RunWithXmlFile(path to file)</b> instead of <b>Run()</b>.</p>

        <p>To get around some of the difficulties of working with XML, the data is loaded into a special object which allows us to query for children or attributes of an element. Before you get all excited and jump to any conclusions though, this is NOT XPath.</p>

        <p>The basic principle of the querying is that anything starting with <b>~</b> is looking for a child element, anything starting with <b>@</b> is looking for an attribute, and anything without one of these prefixes is looking for something that could be either a child element or an attribute.</p>
        
        

        <p Style="SubTitle">Getting Child Elements</p>

        <p>There are 5 options we have to query for child elements:</p>

        <list Type="BulletList1" NumberPosition="5mm">
            <p><b>~First</b> - Returns the first child element</p>
            <p><b>~Last</b> - Returns the last child element</p>
            <p><b>~All</b> - Returns all child elements</p>
            <p><b>~Index</b> - Gets followed by a number to return a specific child</p>
            <p><b>~Value</b> - Returns the text child, getting the value content of the element (this is done automatically when writing elements values to the PDF document, though you will want to explicitly call it if storing or calculating on the contents of an XML element)</p>
        </list>

        <p>All of these (except from ~Value) can be suffixed with an underscore then a name in order to filter the results by child elements only of a certain type. For example:</p>

        <p>This will get the name of the first child within our menu root element:</p>

        <p Style="Code">&lt;p&gt;```{Model.~First.~First_Name}```&lt;/p&gt;</p>

        <p>{Model.~First.~First_Name}</p>

        <p>Meanwhile, this will get the name of the first Dessert child of our menu:</p>
        
        <p Style="Code">&lt;p&gt;```{Model.~First_Dessert.~First_Name}```&lt;/p&gt;</p>

        <p>{Model.~First_Dessert.~First_Name}</p>

        <p>This is clearly a pretty cumbersome way of writing, and we'll be wanting to get the first child element by type a lot, so instead of always having to write <i Style="Code">~First_Name</i>, you can write <i Style="Code">~Name</i> or even just <i Style="Code">Name</i>, though this last one may possibly lead to confusion if you have an attribute &amp; child element that share the same name.</p>

        <p>So the above expressions can actually be written as:</p>

        <p Style="Code">&lt;p&gt;```{Model.~First.Name}```&lt;/p&gt;</p>

        <p Style="Code">&lt;p&gt;```{Model.Dessert.Name}```&lt;/p&gt;</p>



        <p Style="SubTitle">Getting Attributes</p>

        <p>Attributes are far more simple to work with since there are no querying options like there are for child elements. Just use the attribute name, optionally explicitly qualifying it as an attribute with an <b>@</b>, and you're done. For example:</p>

        <p Style="Code">&lt;p&gt;```{Model.~Index1_Main.@Calories}```&lt;/p&gt;</p>

        <p>Produces:</p>

        <p>{Model.~Index1_Main.@Calories}</p>



        <p Style="SubTitle">Putting it all together</p>

        <p>Now we'll create a simple menu to give a working example of using XML data:</p>

        <p Style="Code">
            &lt;p Format.Font.Bold=&quot;true&quot; Format.Alignment=&quot;Center&quot; Format.Font.Size=&quot;14&quot;&gt;The Guilded Goose&lt;/p&gt;

            &lt;p Format.Font.Bold=&quot;true&quot; Format.Font.Size=&quot;12&quot;&gt;Starters&lt;/p&gt;

            &lt;ForEach Var=&quot;starter&quot; In=&quot;Model.~All_Starter&quot;&gt;
            {Space(4)}&lt;p&gt;
            {Space(8)}```&lt;b&gt;{starter.~Name}{(starter.~Vegetarian != null) ? ' (Vegetarian)' : ''} - {starter.~Price.~Value as double #C}&lt;/b&gt; - &lt;i&gt;{starter.@Calories} cals&lt;/i&gt;```
            {Space(4)}&lt;/p&gt;

            {Space(4)}```&lt;p&gt;{starter.~Description}&lt;/p&gt;```
            {Space(4)}&lt;p/&gt;
            &lt;/ForEach&gt;

            &lt;p Format.Font.Bold=&quot;true&quot; Format.Font.Size=&quot;12&quot;&gt;Main Courses&lt;/p&gt;

            &lt;ForEach Var=&quot;main&quot; In=&quot;Model.~All_Main&quot;&gt;
            {Space(4)}&lt;p&gt;
            {Space(8)}```&lt;b&gt;{main.~Name}{(main.~Vegetarian != null) ? ' (Vegetarian)' : ''} - {main.~Price.~Value as double #C}&lt;/b&gt; - &lt;i&gt;{main.@Calories} cals&lt;/i&gt;```
            {Space(4)}&lt;/p&gt;

            {Space(4)}```&lt;p&gt;{main.~Description}&lt;/p&gt;```
            {Space(4)}&lt;p/&gt;
            &lt;/ForEach&gt;

            &lt;p Format.Font.Bold=&quot;true&quot; Format.Font.Size=&quot;12&quot;&gt;Desserts&lt;/p&gt;

            &lt;ForEach Var=&quot;dessert&quot; In=&quot;Model.~All_Dessert&quot;&gt;
            {Space(4)}&lt;p&gt;
            {Space(8)}```&lt;b&gt;{dessert.~Name}{(dessert.~Vegetarian != null) ? ' (Vegetarian)' : ''} - {dessert.~Price.~Value as double #C}&lt;/b&gt; - &lt;i&gt;{dessert.@Calories} cals&lt;/i&gt;```
            {Space(4)}&lt;/p&gt;

            {Space(4)}```&lt;p&gt;{dessert.~Description}&lt;/p&gt;```
            {Space(4)}&lt;p/&gt;
            &lt;/ForEach&gt;
            
        </p>

        <p>See the next page for the result</p>

        <PageBreak/>

        <p Format.Font.Bold="true" Format.Alignment="Center" Format.Font.Size="14">The Guilded Goose</p>

        <p Format.Font.Bold="true" Format.Font.Size="12">Starters</p>
        
        <ForEach Var="starter" In="Model.~All_Starter">
            <p><b>{starter.~Name}{(starter.~Vegetarian != null) ? ' (Vegetarian)' : ''} - {starter.~Price.~Value as double #C}</b> - <i>{starter.@Calories} cals</i></p>
            
            <p>{starter.~Description}</p>
            <p/>
        </ForEach>

        <p Format.Font.Bold="true" Format.Font.Size="12">Main Courses</p>

        <ForEach Var="main" In="Model.~All_Main">
            <p>
                <b>{main.~Name}{(main.~Vegetarian != null) ? ' (Vegetarian)' : ''} - {main.~Price.~Value as double #C}</b> - <i>{main.@Calories} cals</i>
            </p>

            <p>{main.~Description}</p>
            <p/>
        </ForEach>

        <p Format.Font.Bold="true" Format.Font.Size="12">Desserts</p>

        <ForEach Var="dessert" In="Model.~All_Dessert">
            <p>
                <b>{dessert.~Name}{(dessert.~Vegetarian != null) ? ' (Vegetarian)' : ''} - {dessert.~Price.~Value as double #C}</b> - <i>{dessert.@Calories} cals</i>
            </p>

            <p>{dessert.~Description}</p>
            <p/>
        </ForEach>
    </Section>
</Document>