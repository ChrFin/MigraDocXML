<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 2.2 - Tables</p>

        <p>Tables allow you to structure your content into a grid of rows and columns.</p>

        <p Style="Code" Format.SpaceAfter="5mm">
            &lt;Table Borders.Color=&quot;Black&quot;&gt;
            {Space(4)}&lt;Column Width=&quot;3cm&quot;/&gt;
            {Space(4)}&lt;Column Width=&quot;4cm&quot;/&gt;
            
            {Space(4)}&lt;Row C0=&quot;Hello&quot; C1=&quot;world&quot;/&gt;
            {Space(4)}&lt;Row C0=&quot;Howdy&quot; C1=&quot;universe&quot;/&gt;
            &lt;/Table&gt;
            
        </p>

        <Table Borders.Color="Black">
            <Column Width="2cm"/>
            <Column Width="3cm"/>

            <Row C0="Hello" C1="world"/>
            <Row C0="Howdy" C1="universe"/>
        </Table>

        <p>The most important thing to note about creating tables is <b>you must always define your columns before you start adding rows</b>.</p>

        <p>The C0 &amp; C1 attributes of row define the contents of the cells in the first and second columns of that row. We stick to the programming convention that counting indexes starts from 0 rather than 1.</p>

        <p>In the above example I'm using a very shorthand syntax for adding my cell content. This doesn't allow for any individual cell formatting information, letting you just enter text content. I could also define my row as follows, with exactly the same result:</p>

        <p Style="Code">&lt;Row&gt;
            {Space(4)}&lt;C0&gt;Hello&lt;/C0&gt;
            {Space(4)}&lt;C1&gt;world&lt;/C1&gt;
            &lt;/Row&gt;
        </p>

        <p Style="Code" Format.SpaceBefore="5mm">&lt;Row&gt;
            {Space(4)}&lt;C0&gt;
            {Space(8)}&lt;p&gt;Hello&lt;/p&gt;
            {Space(4)}&lt;/C0&gt;
            {Space(4)}&lt;C1&gt;
            {Space(8)}&lt;p&gt;world&lt;/p&gt;
            {Space(4)}&lt;/C1&gt;
            &lt;/Row&gt;
        </p>

        <p Style="Code" Format.SpaceBefore="5mm">&lt;Row&gt;
            {Space(4)}&lt;Cell Index=&quot;0&quot;&gt;Hello&lt;/Cell&gt;
            {Space(4)}&lt;Cell Index=&quot;1&quot;&gt;world&lt;/Cell&gt;
            &lt;/Row&gt;
        </p>

        <p>It is strongly recommended that you use <b>C0</b>, <b>C1</b>, <b>C2</b> etc. for defining cells rather than <b>Cell Index=&quot;x&quot;</b> for a couple of reasons. Firstly, it is far more succinct and easy to read at a glance. Secondly, column formatting depends on it, something we'll cover in a moment.</p>

        <PageBreak/>

        <p Style="SubTitle">Format Attributes</p>

        <p>I won't go into all the attributes available on the Table, Column, Row, Cell nodes since most of them are pretty self explanatory and can be investigated as and when you need them. One set of attributes that does need to be covered though are the ones that start <b>Format</b>.</p>

        <p>These attributes are the same as the ones available on paragraph elements for setting the text formatting. Using one of these attributes defines that every paragraph within that Table/Column/Row/Cell should use those formatting options.</p>

        <p Style="Code" Format.SpaceAfter="5mm">
            &lt;Table Borders.Color=&quot;Black&quot;&gt;
            {Space(4)}&lt;Column Width=&quot;4cm&quot;/&gt;
            {Space(4)}&lt;Column Width=&quot;4cm&quot; Format.Font.Color=&quot;Red&quot;/&gt;
            {Space(4)}&lt;Column Width=&quot;4cm&quot; Format.Font.Color=&quot;Blue&quot;/&gt;
            
            {Space(4)}&lt;Row C0=&quot;black normal&quot; C1=&quot;red normal&quot; C2=&quot;blue normal&quot;/&gt;
            {Space(4)}&lt;Row Format.Font.Bold=&quot;true&quot; C0=&quot;black bold&quot; C1=&quot;red bold&quot; C2=&quot;blue bold&quot;/&gt;
            {Space(4)}&lt;Row Format.Font.Underline=&quot;Single&quot; C0=&quot;black underline&quot; C1=&quot;red underline&quot; C2=&quot;blue underline&quot;/&gt;
            &lt;/Table&gt;
            
        </p>

        <Table Borders.Color="Black">
            <Column Width="4cm"/>
            <Column Width="4cm" Format.Font.Color="Red"/>
            <Column Width="4cm" Format.Font.Color="Blue"/>

            <Row C0="black normal" C1="red normal" C2="blue normal"/>
            <Row Format.Font.Bold="true" C0="black bold" C1="red bold" C2="blue bold"/>
            <Row Format.Font.Underline="Single" C0="black underline" C1="red underline" C2="blue underline"/>
        </Table>
        
    </Section>
</Document>