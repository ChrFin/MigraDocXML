<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 4.0 - Introduction To Styles</p>

        <p>If up to this point you've looked through the XML of any of the lessons, then you may have noticed a glaring omission from the lessons, namely, each of the designs have a bunch of styles in them which haven't been mentioned yet.</p>

        <p>Styles are used to automatically set the attributes of an element, so that, for example, you don't need to explicitly set the font name &amp; size of every paragraph element in your document, you only need to define them once. You can define styles for any element type and from there target any attribute on that element type.</p>

        <p>Below is a simple example of a paragraph style used in this document:</p>

        <p Style="Code">
            &lt;Style Target="Paragraph"&gt;
            {Space(4)}&lt;Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/&gt;
            &lt;/Style&gt;
            
        </p>

        <p>The <b>Target</b> attribute states what element type the style is to be applied to, while each attribute on the <b>Setters</b> element defines an attribute to be set by the style on any elements the style is applied to. So the above style will set all paragraph elements to have 2mm before &amp; after, and use Calibri font.</p>

        <p>The above is an example of an unnamed style, these get applied to all elements which match the Target type. We can also define named styles which only apply to elements which explicitly name it through the <b>Style</b> attribute.</p>

        <p Style="Code">
            &lt;Style Target="Paragraph" Name="BigRed"&gt;
            {Space(4)}&lt;Setters Format.Font.Color="Red" Format.Font.Bold="true" Format.Font.Size="14"/&gt;
            &lt;/Style&gt;

        </p>

        <Style Target="Paragraph" Name="BigRed">
            <Setters Format.Font.Color="Red" Format.Font.Bold="true" Format.Font.Size="14"/>
        </Style>

        <p>Now this:</p>

        <p Style="Code">&lt;p Style="BigRed"&gt;WARNING!&lt;/p&gt;</p>

        <p>Produces this:</p>

        <p Style="BigRed">WARNING!</p>

        <p>An important detail to note here, is that just because the named Style &quot;BigRed&quot; is being applied to the paragraph, that doesn't mean the unnamed style isn't also. The above paragraph still has 2mm spacing before &amp; after, and is still using the Calibri font. Multiple styles can affect one element, with the order in which they get applied being covered in the next lesson.</p>

    </Section>
</Document>