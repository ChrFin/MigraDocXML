<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 2.5 - PointLists</p>

        <p>PointLists allow you to create bulleted or numbered lists. For example:</p>

        <p Style="Code">
            &lt;PointList Type=&quot;BulletList1&quot; NumberPosition=&quot;5mm&quot;&gt;
            {Space(4)}&lt;p&gt;Do laundry&lt;/p&gt;
            {Space(4)}&lt;p&gt;Tidy&lt;/p&gt;
            {Space(4)}&lt;p&gt;Cook&lt;/p&gt;
            &lt;/PointList&gt;

        </p>

        <p>Produces:</p>

        <PointList Type="BulletList1" Format.SpaceBefore="1mm" Format.SpaceAfter="1mm" NumberPosition="6mm">
            <p>Do laundry</p>
            <p>Tidy</p>
            <p>Cook</p>
        </PointList>

        <p>When creating numbered lists, there is always a tab space inserted after the bullet/number which can produce strangely long looking spaces between the points and the text that accompanies them. The NumberPosition attribute is used to control the the position of the bullet/number and bring it in closer to the text.</p>

        <p>You can also nest lists to create a hierarchical structure. Though you need to manually set the indentations of nested lists, otherwise everything appears in line together. The example below demonstrates this, note the use of <b>list</b> which is an alias for <b>PointList</b>.</p>

        <p Style="Code">
            &lt;list Type=&quot;NumberList1&quot; NumberPosition=&quot;6mm&quot;&gt;
            {Space(4)}&lt;p&gt;Introductions&lt;/p&gt;
            {Space(4)}&lt;p&gt;Go through meeting agenda&lt;/p&gt;
            {Space(4)}&lt;p&gt;Old Mrs Pruett made scones for everyone&lt;/p&gt;
            {Space(4)}&lt;p&gt;Discuss the alarming number of squirrels that have started appearing&lt;/p&gt;
            {Space(4)}&lt;list Type=&quot;NumberList3&quot; NumberPosition=&quot;17mm&quot;&gt;
            {Space(8)}&lt;p&gt;Proposal: Leave a trail of nuts into another neighbourhood&lt;/p&gt;
            {Space(8)}&lt;p&gt;Alternative: Put up cat posters to scare them away&lt;/p&gt;
            {Space(4)}&lt;/list&gt;
            {Space(4)}&lt;p&gt;Questions&lt;/p&gt;
            &lt;/list&gt;

        </p>

        <p>Produces:</p>
        
        <list Type="NumberList1" Format.SpaceBefore="1mm" Format.SpaceAfter="1mm" NumberPosition="6mm">
            <p>Introductions</p>
            <p>Go through meeting agenda</p>
            <p>Old Mrs Pruett made scones for everyone</p>
            <p>Discuss the alarming number of squirrels that have started appearing</p>
            <list Type="NumberList3" NumberPosition="17mm">
                <p>Proposal: Leave a trail of nuts into another neighbourhood</p>
                <p>Alternative: Put up cat posters to scare them away</p>
            </list>
            <p>Questions</p>
        </list>
        
    </Section>
</Document>