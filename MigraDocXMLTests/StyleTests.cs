﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class StyleTests
    {
        [Fact]
        public void SimpleStyle()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Style Target=\"p\">" +
                        "<Setters Format.Font.Bold=\"true\"/>" +
                    "</Style>" +
                    "<Section>" +
                        "<p>Test</p>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Paragraph p = section.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.True(p.GetParagraphModel().Format.Font.Bold);
            Assert.Single(p.GetParagraphModel().Elements);
        }

        [Fact]
        public void CascadingStyles()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Style Target=\"p\">" +
                        "<Setters Format.Font.Bold=\"true\"/>" +
                    "</Style>" +
                    "<Section>" +
                        "<Style Target=\"p\">" +
                            "<Setters Format.Font.Underline=\"Single\"/>" +
                        "</Style>" +
                        "<p>Test</p>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Paragraph p = section.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.True(p.GetParagraphModel().Format.Font.Bold);
            Assert.Equal(MigraDoc.DocumentObjectModel.Underline.Single, p.GetParagraphModel().Format.Font.Underline);
            Assert.Single(p.GetParagraphModel().Elements);
        }

        [Fact]
        public void NamedStyleApplied()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Style Target=\"p\" Name=\"bold\">" +
                        "<Setters Format.Font.Bold=\"true\"/>" +
                    "</Style>" +
                    "<Section>" +
                        "<p Style=\"bold\">Test</p>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Paragraph p = section.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.True(p.GetParagraphModel().Format.Font.Bold);
            Assert.Single(p.GetParagraphModel().Elements);
        }

        [Fact]
        public void NamedStyleNotApplied()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Style Target=\"p\" Name=\"bold\">" +
                        "<Setters Format.Font.Bold=\"true\"/>" +
                    "</Style>" +
                    "<Section>" +
                        "<p>Test</p>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Paragraph p = section.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.False(p.GetParagraphModel().Format.Font.Bold);
            Assert.Single(p.GetParagraphModel().Elements);
        }

        [Fact]
        public void NamedStyleShorthandApplied()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Style Target=\"p(bold)\">" +
                        "<Setters Format.Font.Bold=\"true\"/>" +
                    "</Style>" +
                    "<Section>" +
                        "<p Style=\"bold\">Test</p>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Paragraph p = section.Children.OfType<Paragraph>().First();
			
            Assert.True(p.Format.Font.Bold);
        }

        [Fact]
        public void NamedStyleShorthandNotApplied()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Style Target=\"p(bold)\">" +
                        "<Setters Format.Font.Bold=\"true\"/>" +
                    "</Style>" +
                    "<Section>" +
                        "<p>Test</p>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Paragraph p = section.Children.OfType<Paragraph>().First();
			
            Assert.False(p.Format.Font.Bold);
        }

        [Fact]
        public void StylePathApplied()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Style Target=\"TextFrame/p\">" +
                        "<Setters Format.Font.Bold=\"true\"/>" +
                    "</Style>" +
                    "<Section>" +
                        "<TextFrame>" +
                            "<p>Test</p>" +
                        "</TextFrame>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            TextFrame textFrame = section.Children.OfType<TextFrame>().First();
            Paragraph p = textFrame.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.True(p.GetParagraphModel().Format.Font.Bold);
            Assert.Single(p.GetParagraphModel().Elements);
        }

        [Fact]
        public void StylePathNotApplied()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Style Target=\"Section/p\">" +
                        "<Setters Format.Font.Bold=\"true\"/>" +
                    "</Style>" +
                    "<Section>" +
                        "<TextFrame>" +
                            "<p>Test</p>" +
                        "</TextFrame>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            TextFrame textFrame = section.Children.OfType<TextFrame>().First();
            Paragraph p = textFrame.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.False(p.GetParagraphModel().Format.Font.Bold);
            Assert.Single(p.GetParagraphModel().Elements);
        }

        [Fact]
        public void StylePathSingleWildcardApplied()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Style Target=\"Section/_/p\">" +
                        "<Setters Format.Font.Bold=\"true\"/>" +
                    "</Style>" +
                    "<Section>" +
                        "<TextFrame>" +
                            "<p>Test</p>" +
                        "</TextFrame>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            TextFrame textFrame = section.Children.OfType<TextFrame>().First();
            Paragraph p = textFrame.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.True(p.GetParagraphModel().Format.Font.Bold);
            Assert.Single(p.GetParagraphModel().Elements);
        }

        [Fact]
        public void StylePathSingleWildcardNotApplied()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Style Target=\"TextFrame/_/p\">" +
                        "<Setters Format.Font.Bold=\"true\"/>" +
                    "</Style>" +
                    "<Section>" +
                        "<TextFrame>" +
                            "<p>Test</p>" +
                        "</TextFrame>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            TextFrame textFrame = section.Children.OfType<TextFrame>().First();
            Paragraph p = textFrame.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.False(p.GetParagraphModel().Format.Font.Bold);
            Assert.Single(p.GetParagraphModel().Elements);
        }

        [Fact]
        public void StylePathAnyWildcardApplied()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Style Target=\"Document/*/p\">" +
                        "<Setters Format.Font.Bold=\"true\"/>" +
                    "</Style>" +
                    "<Section>" +
                        "<TextFrame>" +
                            "<p>Test</p>" +
                        "</TextFrame>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            TextFrame textFrame = section.Children.OfType<TextFrame>().First();
            Paragraph p = textFrame.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.True(p.GetParagraphModel().Format.Font.Bold);
            Assert.Single(p.GetParagraphModel().Elements);
        }

        [Fact]
        public void StylePathAnyWildcardNotApplied()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Style Target=\"p/*/p\">" +
                        "<Setters Format.Font.Bold=\"true\"/>" +
                    "</Style>" +
                    "<Section>" +
                        "<TextFrame>" +
                            "<p>Test</p>" +
                        "</TextFrame>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            TextFrame textFrame = section.Children.OfType<TextFrame>().First();
            Paragraph p = textFrame.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.False(p.GetParagraphModel().Format.Font.Bold);
            Assert.Single(p.GetParagraphModel().Elements);
        }

        [Fact]
        public void MultipleNamedStyles()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Style Target=\"p\" Name=\"bold\">" +
                        "<Setters Format.Font.Bold=\"true\"/>" +
                    "</Style>" +
                    "<Style Target=\"p\" Name=\"underline\">" +
                        "<Setters Format.Font.Underline=\"Single\"/>" +
                    "</Style>" +
                    "<Section>" +
                        "<TextFrame>" +
                            "<p Style=\"bold underline\">Test</p>" +
                        "</TextFrame>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            TextFrame textFrame = section.Children.OfType<TextFrame>().First();
            Paragraph p = textFrame.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.True(p.GetParagraphModel().Format.Font.Bold);
            Assert.Equal(MigraDoc.DocumentObjectModel.Underline.Single, p.GetParagraphModel().Format.Font.Underline);
            Assert.Single(p.GetParagraphModel().Elements);
        }

        [Fact]
        public void StylePathIgnoreLogicalElement()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Style Target=\"Section/p\">" +
                        "<Setters Format.Font.Bold=\"true\"/>" +
                    "</Style>" +
                    "<Section>" +
                        "<If Test=\"true\">" +
                            "<p>Test</p>" +
                        "</If>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            If ifElem = section.Children.OfType<If>().First();
            Paragraph p = ifElem.Children.OfType<Paragraph>().First();

            Assert.Equal("Test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.True(p.GetParagraphModel().Format.Font.Bold);
            Assert.Single(p.GetParagraphModel().Elements);
        }

		[Fact]
		public void MultiPartStyleDefinition()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Style Target=\"Section/p|Row\">" +
						"<Setters Format.Font.Bold=\"true\"/>" +
					"</Style>" +
					"<Section>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Style style = doc.Children.OfType<Style>().First();

			Assert.Equal(2, style.TargetPaths.Count);

			Assert.Equal(2, style.TargetPaths[0].Count);
			Assert.Equal(typeof(Section), (style.TargetPaths[0][0] as StyledType).Type);
			Assert.Equal(typeof(Paragraph), (style.TargetPaths[0][1] as StyledType).Type);

			Assert.Single(style.TargetPaths[1]);
			Assert.Equal(typeof(Row), (style.TargetPaths[1][0] as StyledType).Type);
		}

		[Fact]
		public void MultiPartStyleApplied()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Style Target=\"p(a)|p(b)\">" +
						"<Setters Format.Font.Bold=\"true\"/>" +
					"</Style>" +
					"<Section>" +
						"<p>hello</p>" +
						"<p Style=\"a\">hello</p>" +
						"<p Style=\"b\">hello</p>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Paragraph p = section.Children.OfType<Paragraph>().First(x => string.IsNullOrEmpty(x.Style));
			Paragraph pa = section.Children.OfType<Paragraph>().First(x => x.Style == "a");
			Paragraph pb = section.Children.OfType<Paragraph>().First(x => x.Style == "b");

			Assert.False(p.Format.Font.Bold);
			Assert.True(pa.Format.Font.Bold);
			Assert.True(pb.Format.Font.Bold);
		}
    }
}
