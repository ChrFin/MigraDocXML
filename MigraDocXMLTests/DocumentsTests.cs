﻿using MigraDocXML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MigraDocXMLTests
{
	public class DocumentsTests
	{
		[Fact]
		public void MultipleDocumentsThrowsErrorFromRun()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Documents>" +
					"<Document>" +
						"<Section>" +
							"<p>Hello</p>" +
						"</Section>" +
					"</Document>" +
					"<Document>" +
						"<Section>" +
							"<p>Goodbye</p>" +
						"</Section>" +
					"</Document>" +
				"</Documents>";

			Assert.Throws<Exception>(() => new PdfXmlReader() { DesignText = code }.Run());
		}

		[Fact]
		public void MultipleDocumentsDoesntThrowErrorFromRender()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Documents>" +
					"<Document>" +
						"<Section>" +
							"<p>Hello</p>" +
						"</Section>" +
					"</Document>" +
					"<Document>" +
						"<Section>" +
							"<p>Goodbye</p>" +
						"</Section>" +
					"</Document>" +
				"</Documents>";

			var pdfDoc = new PdfXmlReader() { DesignText = code }.Render();
			Assert.NotNull(pdfDoc);
		}
	}
}
