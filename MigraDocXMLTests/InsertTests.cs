﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class InsertTests
    {
        [Fact]
        public void LocalInsert()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Resource Name=\"myResource\">" +
                        "<p>Resource test</p>" +
                    "</Resource>" +
                    "<Section>" +
                        "<Insert Resource=\"myResource\"/>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Insert insert = section.Children.OfType<Insert>().First();

            Assert.Equal(2, section.Children.Count());
			Assert.Empty(insert.Children);
			Assert.IsType<Insert>(section.Children.First());
            Assert.IsType<Paragraph>(section.Children.Last());
            
            Assert.Equal(
                "Resource test", 
                section.GetSectionModel()
                    .Elements.OfType<MigraDoc.DocumentObjectModel.Paragraph>().First()
                    .Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First()
                    .Content
            );
        }


        [Fact]
        public void RemoteInsert()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document ResourcePath=\"..\\..\\\">" +
                    "<Section>" +
                        "<Insert Path=\"remoteresource.xml\" Resource=\"myResource\"/>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Insert insert = section.Children.OfType<Insert>().First();

			Assert.Equal(2, section.Children.Count());
            Assert.Empty(insert.Children);
			Assert.IsType<Insert>(section.Children.First());
            Assert.IsType<Paragraph>(section.Children.Last());

            Assert.Equal(
                "This is a remote resource",
                section.GetSectionModel()
                    .Elements.OfType<MigraDoc.DocumentObjectModel.Paragraph>().First()
                    .Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First()
                    .Content
            );
        }


		[Fact]
		public void TextInsert()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<p><Insert Text=\"2 + 2 = {2 + 2}\"/></p>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Paragraph p = section.Children.OfType<Paragraph>().First();
			Insert insert = p.Children.OfType<Insert>().First();
			
			Assert.Empty(insert.Children);

			Assert.Equal(
				"2 + 2 = 4",
				p.GetParagraphModel()
					.Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First()
					.Content
			);
		}


		[Fact]
		public void InsertOverwritesVariables()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Resource Name=\"testResource\">" +
						"<p>{testVar}</p>" +
					"</Resource>" +
					"<Section>" +
						"<Var testVar=\"1\"/>" +
						"<p>{testVar}</p>" +
						"<Insert Resource=\"testResource\" testVar=\"2\"/>" +
						"<p>{testVar}</p>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			List<Paragraph> ps = section.Children.OfType<Paragraph>().ToList();

			Assert.Equal(3, ps.Count);
			Assert.Equal("1", ps[0].GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
			Assert.Equal("2", ps[1].GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
			Assert.Equal("1", ps[2].GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
		}
    }
}
