﻿using MigraDocXML;
using MigraDocXML.DOM;
using MigraDocXML.DOM.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class BezierTests
    {
        [Fact]
        public void PageInheritedFromGraphics()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Graphics Page=\"3\">" +
                            "<Bezier/>" +
                        "</Graphics>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Graphics gfx = section.Children.OfType<Graphics>().First();
            gfx.ChildProcessor();
            Bezier bezier = gfx.Children.OfType<Bezier>().First();

            Assert.Equal(3, gfx.Page);
            Assert.Equal(3, bezier.Page);
        }

        [Fact]
        public void GraphicsPageOverwritten()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Graphics Page=\"3\">" +
                            "<Bezier Page=\"7\"/>" +
                        "</Graphics>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Graphics gfx = section.Children.OfType<Graphics>().First();
            gfx.ChildProcessor();
            Bezier bezier = gfx.Children.OfType<Bezier>().First();

            Assert.Equal(3, gfx.Page);
            Assert.Equal(7, bezier.Page);
        }

        [Fact]
        public void PointsAdded()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Graphics Page=\"3\">" +
                            "<Bezier>" +
                                "<Point X=\"1cm\" Y=\"2cm\"/>" +
                                "<Point X=\"3cm\" Y=\"4cm\"/>" + 
                                "<Point X=\"5cm\" Y=\"2cm\"/>" +
                                "<Point X=\"7cm\" Y=\"4cm\"/>" +
                            "</Bezier>" +
                        "</Graphics>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Graphics gfx = section.Children.OfType<Graphics>().First();
            gfx.ChildProcessor();
            Bezier bezier = gfx.Children.OfType<Bezier>().First();
            var points = bezier.Children.OfType<Point>().ToList();

            Assert.Equal(4, points.Count);
            Assert.Equal(bezier, points[0].GetGraphicalParent());
            Assert.Equal(bezier, points[1].GetGraphicalParent());
            Assert.Equal(bezier, points[2].GetGraphicalParent());
            Assert.Equal(bezier, points[3].GetGraphicalParent());
        }
    }
}
