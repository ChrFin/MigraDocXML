﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class TableTests
    {
        [Fact]
        public void SimpleTable()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Table>" +
                            "<Column Width=\"5cm\"/>" +
                            "<Column Width=\"5cm\"/>" +
                            "<Row C0=\"Hello\" C1=\"World\"/>" +
                            "<Row C0=\"Howdy\" C1=\"Universe\"/>" +
                        "</Table>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Table tbl = section.Children.OfType<Table>().First();

            Assert.Equal(2, tbl.GetTableModel().Columns.Count);
            Assert.Equal(2, tbl.GetTableModel().Rows.Count);
            Assert.Equal(section.GetSectionModel(), tbl.GetTableModel().Section);

            Assert.Equal(
                "Hello",
                tbl.GetTableModel().Rows[0].Cells[0].Elements
                    .OfType<MigraDoc.DocumentObjectModel.Paragraph>().First()
                    .Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First()
                    .Content
            );
            Assert.Equal(
                "World",
                tbl.GetTableModel().Rows[0].Cells[1].Elements
                    .OfType<MigraDoc.DocumentObjectModel.Paragraph>().First()
                    .Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First()
                    .Content
            );
            Assert.Equal(
                "Howdy",
                tbl.GetTableModel().Rows[1].Cells[0].Elements
                    .OfType<MigraDoc.DocumentObjectModel.Paragraph>().First()
                    .Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First()
                    .Content
            );
            Assert.Equal(
                "Universe",
                tbl.GetTableModel().Rows[1].Cells[1].Elements
                    .OfType<MigraDoc.DocumentObjectModel.Paragraph>().First()
                    .Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First()
                    .Content
            );
        }

        [Fact]
        public void StyledColumn()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Table>" +
                            "<Column Width=\"5cm\" Format.Font.Bold=\"true\"/>" +
                            "<Column Width=\"5cm\"/>" +
                            "<Row C0=\"Hello\" C1=\"World\"/>" +
                            "<Row C0=\"Howdy\" C1=\"Universe\"/>" +
                        "</Table>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Table tbl = section.Children.OfType<Table>().First();

            Assert.Single(tbl.Children.OfType<Style>());
            var style = tbl.Children.OfType<Style>().First();
            Assert.Equal(7, style.TargetPaths[0].Count);
            Assert.Equal(typeof(Document), (style.TargetPaths[0][0] as StyledType).Type);
            Assert.Equal(typeof(Section), (style.TargetPaths[0][1] as StyledType).Type);
            Assert.Equal(typeof(Table), (style.TargetPaths[0][2] as StyledType).Type);
            Assert.Equal(typeof(Row), (style.TargetPaths[0][3] as StyledType).Type);
            Assert.Equal(typeof(C0), (style.TargetPaths[0][4] as StyledType).Type);
            Assert.Equal(-1, (style.TargetPaths[0][5] as StyleTargetWildcard).Count);
            Assert.Equal(typeof(Paragraph), (style.TargetPaths[0][6] as StyledType).Type);
        }

        [Fact]
        public void CellWithAttributes()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Table>" +
                            "<Column Width=\"5cm\"/>" +
                            "<Row>" +
                                "<C0 VerticalAlignment=\"Bottom\">Hello</C0>" +
                            "</Row>" +
                        "</Table>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Table tbl = section.Children.OfType<Table>().First();
            Row row = tbl.Children.OfType<Row>().First();
            Cell cell = row.Children.OfType<Cell>().First();

            Assert.Equal(MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Bottom, cell.GetCellModel().VerticalAlignment);
            Assert.Equal(
                "Hello",
                cell.GetCellModel().Elements
                    .OfType<MigraDoc.DocumentObjectModel.Paragraph>().First()
                    .Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First()
                    .Content
            );
        }

        [Fact]
        public void TableParagraphFormatting()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Table Format.LeftIndent=\"1cm\"/>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Table tbl = section.Children.OfType<Table>().First();
            Style tblStyle = tbl.Children.OfType<Style>().First();
            Setters setters = tblStyle.Setters;

            Assert.Single(setters.GetItems());
            Assert.Equal("1cm", setters.GetItems()["Format.LeftIndent"]);
        }

		[Fact]
		public void ColumnUsingTableStyle()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<Table>" +
							"<Style Name=\"test\" Target=\"Column\">" +
								"<Setters Format.Font.Bold=\"true\"/>" +
							"</Style>" +
							"<Column Width=\"5cm\" Style=\"test\"/>" +
							"<Row C0=\"hello\"/>" +
						"</Table>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Table tbl = section.Children.OfType<Table>().First();
			Row row = tbl.Children.OfType<Row>().First();
			Cell c0 = row.Children.OfType<C0>().First();
			Paragraph p = c0.Children.OfType<Paragraph>().First();

			Assert.True(p.Format.Font.Bold);
		}

		[Fact]
		public void TextFrameInsideCell()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<Table>" +
							"<Column Width=\"5cm\"/>" +
							"<Row>" +
								"<C0>" +
									"<TextFrame>" +
										"<p>Hello</p>" +
									"</TextFrame>" +
								"</C0>" +
							"</Row>" +
						"</Table>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Table tbl = section.Children.OfType<Table>().First();
			Row row = tbl.Children.OfType<Row>().First();
			Cell c0 = row.Children.OfType<C0>().First();
			TextFrame frame = c0.Children.OfType<TextFrame>().First();
			Paragraph p = frame.Children.OfType<Paragraph>().First();

			Assert.Equal(
				"Hello",
				p.GetParagraphModel()
					.Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First()
					.Content
			);
		}
    }
}
