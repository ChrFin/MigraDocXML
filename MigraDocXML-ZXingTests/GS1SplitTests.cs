﻿using MigraDocXML_ZXing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MigraDocXML_ZXingTests
{
	public class GS1SplitTests
	{
		[Fact]
		public void SingleAI()
		{
			var output = GS1.SplitReadableText("(01)12345");
			Assert.Single(output);
			Assert.Equal("01", output[0].Key);
			Assert.Equal("12345", output[0].Value);
		}

		[Fact]
		public void MultipleAIs()
		{
			var output = GS1.SplitReadableText("(01)12345(02)98765");
			Assert.Equal(2, output.Count);
			Assert.Equal("01", output[0].Key);
			Assert.Equal("12345", output[0].Value);
			Assert.Equal("02", output[1].Key);
			Assert.Equal("98765", output[1].Value);
		}

		[Fact]
		public void NoBrackets()
		{
			Assert.Throws<Exception>(() => GS1.SplitReadableText("0112345"));
		}

		[Fact]
		public void NoClosingBracket()
		{
			Assert.Throws<Exception>(() => GS1.SplitReadableText("(0112345"));
		}

		[Fact]
		public void MultipleClosingBrackets()
		{
			Assert.Throws<Exception>(() => GS1.SplitReadableText("(01))12345"));
		}

		[Fact]
		public void RepeatedAICode()
		{
			Assert.Throws<Exception>(() => GS1.SplitReadableText("(01)12345(01)56789"));
		}
	}
}
