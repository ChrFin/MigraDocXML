﻿using MigraDocXML_ZXing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MigraDocXML_ZXingTests
{
	public class Code128Tests
	{
		[Fact]
		public void SetGS1TextValueWithGS1Format()
		{
			var code128 = new Code128();
			code128.GS1Format = true;
			code128.SetTextValue("(11)123456(21)9999");
			Assert.Equal($"{(char)241}11123456219999", code128.Contents);
		}

		[Fact]
		public void SetGS1TextValueWithoutGS1Format()
		{
			var code128 = new Code128();
			code128.GS1Format = false;
			code128.SetTextValue("(11)123456(21)9999");
			Assert.Equal("(11)123456(21)9999", code128.Contents);
		}

		[Fact]
		public void SetNormalTextValueWithGS1Format()
		{
			var code128 = new Code128();
			code128.GS1Format = true;
			code128.SetTextValue("ABCDE");
			Assert.Equal("ABCDE", code128.Contents);
		}

		[Fact]
		public void UnrecognisedAICode()
		{
			var code128 = new Code128();
			code128.GS1Format = true;
			Assert.Throws<Exception>(() => code128.SetTextValue("(03)9999"));
		}

		[Fact]
		public void WrongAIDataLength()
		{
			var code128 = new Code128();
			code128.GS1Format = true;
			Assert.Throws<Exception>(() => code128.SetTextValue("(02)9999"));
		}

		[Fact]
		public void VariableCodeEnd()
		{
			var code128 = new Code128();
			code128.GS1Format = true;
			code128.SetTextValue("(3110)123456");
			Assert.Equal($"{(char)241}3110123456", code128.Contents);
		}
	}
}
