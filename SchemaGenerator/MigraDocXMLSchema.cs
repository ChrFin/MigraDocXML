﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemaGenerator
{
    public class MigraDocXMLSchema : Schema
    {

        public List<ElementBase> LogicalElements { get; private set; } = new List<ElementBase>()
        {
            new Element("ForEach", "forEachType"),
            new Element("While", "logicalTestType"),
            new Element("Var", "varType"),
            new Element("Set", "setType"),
            new Element("Style", "styleType"),
            new Element("Quit", "emptyType"),
            new Element("Break", "emptyType"),
            new Element("Continue", "emptyType"),
            new Element("Insert", "insertType"),
            new SequenceElement()
                .Add(new Element("If", "logicalTestType", 1, 1))
                .Add(new Element("ElseIf", "logicalTestType", 0, int.MaxValue))
                .Add(new Element("Else", "logicalType", 0, 1))
        };

        public List<ElementBase> ParagraphElements { get; private set; } = new List<ElementBase>()
        {
            new Element("Paragraph", "paragraphType"),
            new Element("p", "paragraphType")
        };

        public List<ElementBase> HyperlinkElements { get; private set; } = new List<ElementBase>()
        {
            new Element("Hyperlink", "hyperlinkType"),
            new Element("a", "hyperlinkType")
        };

        public List<ElementBase> PointListElements { get; private set; } = new List<ElementBase>()
        {
            new Element("PointList", "pointListType"),
            new Element("list", "pointListType")
        };

        public List<ElementBase> ParagraphFormattingElements { get; private set; } = new List<ElementBase>()
        {
            new Element("FormattedText", "formattedTextType"),
            new Element("b", "formattedTextType"),
            new Element("i", "formattedTextType"),
            new Element("ul", "formattedTextType"),
            new Element("sub", "formattedTextType"),
            new Element("super", "formattedTextType"),
            new Element("Bookmark", "bookmarkType")
        };

        public List<ElementBase> GraphicsElements { get; private set; } = new List<ElementBase>()
        {
            new Element("Line", "graphicsLineType"),
            new Element("Bezier", "graphicsBezierType"),
            new Element("Rect", "graphicsRectType"),
			new Element("String", "graphicsStringType"),
			new Element("TextAnnotation", "textAnnotationType")
        };

        public List<ElementBase> ParagraphFieldElements = new List<ElementBase>()
        {
            new Element("DateField", "dateFieldType"),
            new Element("PageField", "pageFieldType"),
            new Element("NumPagesField", "numPagesFieldType")
        };

        public List<ElementBase> ImageElements = new List<ElementBase>()
        {
            new Element("Image", "imageType")
        };

        public List<Attribute> BordersAttributes = new List<Attribute>()
        {
            new Attribute("Borders.Bottom.Color", "colorType"),
            new Attribute("Borders.Bottom.Name", "xs:string"),
            new Attribute("Borders.Bottom.Visible", "xs:string"),
            new Attribute("Borders.Bottom.Width", "xs:string"),

            new Attribute("Borders.Left.Color", "colorType"),
            new Attribute("Borders.Left.Name", "xs:string"),
            new Attribute("Borders.Left.Visible", "xs:string"),
            new Attribute("Borders.Left.Width", "xs:string"),

            new Attribute("Borders.Right.Color", "colorType"),
            new Attribute("Borders.Right.Name", "xs:string"),
            new Attribute("Borders.Right.Visible", "xs:string"),
            new Attribute("Borders.Right.Width", "xs:string"),

            new Attribute("Borders.Top.Color", "colorType"),
            new Attribute("Borders.Top.Name", "xs:string"),
            new Attribute("Borders.Top.Visible", "xs:string"),
            new Attribute("Borders.Top.Width", "xs:string"),

            new Attribute("Borders.DiagonalDown.Color", "colorType"),
            new Attribute("Borders.DiagonalDown.Name", "xs:string"),
            new Attribute("Borders.DiagonalDown.Visible", "xs:string"),
            new Attribute("Borders.DiagonalDown.Width", "xs:string"),

            new Attribute("Borders.DiagonalUp.Color", "colorType"),
            new Attribute("Borders.DiagonalUp.Name", "xs:string"),
            new Attribute("Borders.DiagonalUp.Visible", "xs:string"),
            new Attribute("Borders.DiagonalUp.Width", "xs:string"),

            new Attribute("Borders.Color", "colorType"),
            new Attribute("Borders.Distance", "xs:string"),
            new Attribute("Borders.Visible", "xs:string"),
            new Attribute("Borders.Width", "xs:string"),
        };

        public List<Attribute> ShadingAttributes = new List<Attribute>()
        {
            new Attribute("Shading.Color", "colorType"),
            new Attribute("Shading.Visible", "boolType")
        };

        public List<Attribute> LineFormatAttributes = new List<Attribute>()
        {
            new Attribute("LineFormat.Color", "colorType"),
            new Attribute("LineFormat.DashStyle", "dashStyleType"),
            new Attribute("LineFormat.Visible", "boolType"),
            new Attribute("LineFormat.Width", "unitType")
        };

        public List<Attribute> FillFormatAttributes = new List<Attribute>()
        {
            new Attribute("FillFormat.Color", "colorType"),
            new Attribute("FillFormat.Visible", "boolType")
        };

        public List<Attribute> FontAttributes = new List<Attribute>()
        {
            new Attribute("Font.Bold", "boolType"),
            new Attribute("Font.Italic", "boolType"),
            new Attribute("Font.Size", "unitType"),
            new Attribute("Font.Color", "colorType"),
            new Attribute("Font.Name", "xs:string"),
            new Attribute("Font.Subscript", "boolType"),
            new Attribute("Font.Superscript", "boolType"),
            new Attribute("Font.Underline", "underlineType")
        };

        public List<Attribute> PenAttributes = new List<Attribute>()
        {
            new Attribute("Pen.Color", "colorType"),
            new Attribute("Pen.DashOffset", "doubleType"),
            new Attribute("Pen.DashStyle", "graphicsDashStyleType"),
            new Attribute("Pen.LineCap", "graphicsLineCapType"),
            new Attribute("Pen.LineJoin", "graphicsLineJoinType"),
            new Attribute("Pen.MiterLimit", "doubleType"),
            new Attribute("Pen.Width", "doubleType")
        };



        public MigraDocXMLSchema()
        {
        }

        public MigraDocXMLSchema Build()
        {
            XmlArgs["version"] = "1.0";

            SchemaArgs["elementFormDefault"] = "qualified";
            SchemaArgs["xmlns:xs"] = "http://www.w3.org/2001/XMLSchema";
            
            var styleAttribute = new Attribute("Style", "xs:string");
            
            Types.Add(new SimpleType("unitType")
                .RestrictByPattern("xs:string", @"([-?\d.]+(mm|cm|in|pt|pc|))|(.*{.*}.*)")
            );

            Types.Add(new SimpleType("scriptType")
                .RestrictByPattern("xs:string", @"(.*{.*}.*)")
            );

            Types.Add(new SimpleType("intType")
                .RestrictByPattern("xs:string", @"(-?\d+)|(.*{.*}.*)")
            );

            Types.Add(new SimpleType("boolType")
                .RestrictByPattern("xs:string", "true|false|0|1|(.*{.*}.*)")
            );

            Types.Add(new SimpleType("doubleType")
                .RestrictByPattern("xs:string", @"(-?\d+\.?\d*)|(.*{.*}.*)")
            );

            Types.Add(new SimpleType("orientationType")
                .RestrictByEnumeration("xs:string", "Landscape", "Portrait")
            );

            Types.Add(new SimpleType("pageFormatType")
                .RestrictByEnumeration("xs:string", "A0", "A1", "A2", "A3", "A4", "A5", "A6", "B5", "Ledger", "Legal", "Letter", "P11x17")
            );

            Types.Add(new SimpleType("breakTypeType")
                .RestrictByEnumeration("xs:string", "BreakEvenPage", "BreakNextPage", "BreakOddPage")
            );

            Types.Add(new SimpleType("paragraphAlignmentType")
                .RestrictByEnumeration("xs:string", "Center", "Justify", "Left", "Right")
            );

            Types.Add(new SimpleType("lineSpacingRuleType")
                .RestrictByEnumeration("xs:string", "AtLeast", "Double", "Exactly", "Multiple", "OnePtFive", "Single")
            );

            Types.Add(new SimpleType("colorRegexType")
                .RestrictByPattern("xs:string", @"(#[0-9A-Fa-f]{6})|(#[0-9A-Fa-f]{8})|(.*{.*}.*)")
            );

            Types.Add(new SimpleType("colorListType")
                .RestrictByEnumeration("xs:string",
                    "AliceBlue", "AntiqueWhite", "Aqua", "Aquamarine", "Azure",
                    "Beige", "Bisque", "Black", "BlanchedAlmond", "Blue",
                    "BlueViolet", "Brown", "BurlyWood", "CadetBlue", "Chartreuse",
                    "Chocolate", "Coral", "CornflowerBlue", "Cornsilk", "Crimson",
                    "Cyan", "DarkBlue", "DarkCyan", "DarkGoldenrod", "DarkGray",
                    "DarkGreen", "DarkKhaki", "DarkMagenta", "DarkOliveGreen", "DarkOrange",
                    "DarkOrchid", "DarkRed", "DarkSalmon", "DarkSeaGreen", "DarkSlateBlue",
                    "DarkSlateGray", "DarkTurquoise", "DarkViolet", "DeepPink", "DeepSkyBlue",
                    "DimGray", "DodgerBlue", "Firebrick", "FloaralWhite", "ForestGreen",
                    "Fuchsia", "Gainsboro", "GhostWhite", "Gold", "Goldenrod",
                    "Gray", "Green", "GreenYellow", "Honeydew", "HotPink",
                    "IndianRed", "Indigo", "Ivory", "Khaki", "Lavender",
                    "LavenderBlush", "LawnGreen", "LemonChiffon", "LightBlue", "LightCoral",
                    "LightCyan", "LightGoldenrodYellow", "LightGray", "LightGreen", "LightPink",
                    "LightSalmon", "LightSeaGreen", "LightSkyBlue", "LightSlateGray", "LightSteelBlue",
                    "LightYellow", "Lime", "LimeGreen", "Linen", "Magenta",
                    "Maroon", "MediumAquamarine", "MediumBlue", "MediumOrchid", "MediumPurple",
                    "MediumSeaGreen", "MediumSlateBlue", "MediumSpringGreen", "MediumTurquoise", "MediumVioletRed",
                    "MidnightBlue", "MintCream", "MistyRose", "Moccasin", "NavajoWhite",
                    "Navy", "OldLace", "Olive", "OliveDrab", "Orange",
                    "OrangeRed", "Orchid", "PaleGoldenrod", "PaleGreen", "PaleTurquoise",
                    "PaleVioletRed", "PapayaWhip", "PeachPuff", "Peru", "Pink",
                    "Plum", "PowderBlue", "Purple", "Red", "RosyBrown",
                    "RoyalBlue", "SaddleBrown", "Salmon", "SandyBrown", "SeaGreen",
                    "SeaShell", "Sienna", "Silver", "SkyBlue", "SlateBlue",
                    "SlateGray", "Snow", "SpringGreen", "SteelBlue", "Tan",
                    "Teal", "Thistle", "Tomato", "Transparent", "Turquoise",
                    "Violet", "Wheat", "White", "WhiteSmoke", "Yellow",
                    "YellowGreen"
                )
            );

            Types.Add(new SimpleType("colorType")
                .RestrictByUnion("colorRegexType", "colorListType")
            );

            Types.Add(new SimpleType("outlineLevelType")
                .RestrictByEnumeration("xs:string", "BodyText", "Level1", "Level2", "Level3", "Level4", "Level5", "Level6", "Level7", "Level8", "Level9")
            );

            Types.Add(new SimpleType("underlineType")
                .RestrictByEnumeration("xs:string", "Dash", "DotDash", "DotDotDash", "Dotted", "None", "Single", "Words")
            );

            Types.Add(new SimpleType("hyperlinkTypeType")
                .RestrictByEnumeration("xs:string", "Bookmark", "File", "Local", "Url", "Web")
            );

            Types.Add(new SimpleType("relativeHorizontalType")
                .RestrictByEnumeration("xs:string", "Character", "Column", "Margin", "Page")
            );

            Types.Add(new SimpleType("relativeVerticalType")
                .RestrictByEnumeration("xs:string", "Line", "Margin", "Page", "Paragraph")
            );

            Types.Add(new SimpleType("horizontalAlignmentType")
                .RestrictByEnumeration("xs:string", "Left", "Center", "Right")
            );

            Types.Add(new SimpleType("verticalAlignmentType")
                .RestrictByEnumeration("xs:string", "Bottom", "Center", "Top")
            );

            Types.Add(new SimpleType("rowHeightRuleType")
                .RestrictByEnumeration("xs:string", "AtLeast", "Auto", "Exactly")
            );

            Types.Add(new SimpleType("footnoteLocationType")
                .RestrictByEnumeration("xs:string", "BeneathText", "BottomOfPage")
            );

            Types.Add(new SimpleType("footnoteNumberingRuleType")
                .RestrictByEnumeration("xs:string", "RestartContinuous", "RestartPage", "RestartSection")
            );

            Types.Add(new SimpleType("footnoteNumberStyleType")
                .RestrictByEnumeration("xs:string", "Arabic", "LowercaseLetter", "LowercaseRoman", "UppercaseLetter", "UppercaseRoman")
            );

            Types.Add(new SimpleType("dashStyleType")
                .RestrictByEnumeration("xs:string", "Dash", "DashDot", "DashDotDot", "Solid", "SquareDot")
            );

            Types.Add(new SimpleType("textOrientationType")
                .RestrictByEnumeration("xs:string", "Downward", "Horizontal", "HorizontalRotatedFarEast", "Upward", "Vertical", "VerticalFarEast")
            );

            Types.Add(new SimpleType("shapePositionType")
                .RestrictByEnumeration("xs:string", "Bottom", "Center", "Inside", "Left", "Outside", "Right", "Top", "Undefined")
            );

            Types.Add(new SimpleType("wrapStyleType")
                .RestrictByEnumeration("xs:string", "None", "Through", "TopBottom")
            );

            Types.Add(new SimpleType("pointListTypeType")
                .RestrictByEnumeration("xs:string", "BulletList1", "BulletList2", "BulletList3", "NumberList1", "NumberList2", "NumberList3")
            );

			Types.Add(new SimpleType("textAnnotationIconType")
				.RestrictByEnumeration("xs:string", "NoIcon", "Comment", "Help", "Insert", "Key", "NewParagraph", "Note", "Paragraph")
			);


            Types.Add(new ComplexType("varType")
                .Add(new AnyAttribute("skip"))
            );

            Types.Add(new ComplexType("setType")
                .Add(new AnyAttribute("skip"))
            );

            Types.Add(new ComplexType("defaultType")
                .Add(new AnyAttribute("skip"))
            );

            Types.Add(new ComplexType("emptyType")
            );

            Types.Add(new ComplexType("settersType")
                .Add(new AnyAttribute("lax"))
            );

            Types.Add(new ComplexType("styleType")
                .Add(new Attribute("Target", "xs:string", use: "required"))
                .Add(new Attribute("Name", "xs:string"))
                .Add(new SequenceElement())
                    .Add(new Element("Setters", "settersType", 1, 1)
                )
            );
            
            Types.Add(new ComplexType("bookmarkType")
                .ExtendsSimple("xs:string")
                .Add(new Attribute("Name", "xs:string"))
            );
            
            Types.Add(new ComplexType("documentType")
                .Add(new Attribute("FootnoteLocation", "footnoteLocationType"))
                .Add(new Attribute("FootnoteNumberingRule", "footnoteNumberingRuleType"))
                .Add(new Attribute("FootnoteNumberStyle", "footnoteNumberStyleType"))
                .Add(new Attribute("FootnoteStartingNumber", "intType"))
                .Add(new Attribute("ImagePath", "xs:string"))
                .Add(new Attribute("ResourcePath", "xs:string"))
                .Add(new Attribute("UseCmykColor", "boolType"))
				.Add(new Attribute("Name", "xs:string"))
				.Add(new Attribute("Tag", "xs:string"))
                .Add(new ChoiceElement(0, int.MaxValue)
                    .Add(LogicalElements)
                    .Add(new Element("Graphics", "graphicsType"))
                    .Add(new Element("Section", "sectionType"))
                    .Add(new Element("Resource", "resourceType"))
                )
            );

			Types.Add(new ComplexType("documentsType")
				.Add(new Attribute("ImagePath", "xs:string"))
				.Add(new Attribute("ResourcePath", "xs:string"))
				.Add(new SequenceElement()
					.Add(new ChoiceElement(1, int.MaxValue)
						.Add(LogicalElements)
						.Add(new Element("Document", "documentType"))
					)
					.Add(new Element("Output", "outputType", 1, 1))
				)
			);

			Types.Add(new ComplexType("outputType")
				.Add(new ChoiceElement(1, int.MaxValue)
					.Add(LogicalElements)
					.Add(new Element("Page", "pageType"))
				)
			);

			Types.Add(new ComplexType("pageType")
				.Add(new Attribute("Width", "unitType"))
				.Add(new Attribute("Height", "unitType"))
				.Add(new Attribute("Orientation", "orientationType"))
				.Add(new ChoiceElement(1, int.MaxValue)
					.Add(LogicalElements)
					.Add(new Element("Area", "areaType"))
				)	
			);

			Types.Add(new ComplexType("areaType")
				.Add(new Attribute("X", "unitType"))
				.Add(new Attribute("Y", "unitType"))
				.Add(new Attribute("Width", "unitType"))
				.Add(new Attribute("Height", "unitType"))
				.Add(new ChoiceElement(1, int.MaxValue)
					.Add(LogicalElements)
					.Add(new Element("SourcePage", "sourcePageType"))
				)
			);

			Types.Add(new ComplexType("sourcePageType")
				.Add(new Attribute("Document", "xs:string"))
				.Add(new Attribute("Page", "intType"))
			);

            Types.Add(new ComplexType("resourceType")
                .Add(new Attribute("Name", "xs:string", use: "required"))
                .Add(new ChoiceElement(0, int.MaxValue)
                    .Add(LogicalElements)
                    .Add(new Element("Default", "defaultType"))
                    .Add(ParagraphElements)
                    .Add(HyperlinkElements)
                    .Add(ParagraphFormattingElements)
                    .Add(PointListElements)
                    .Add(ImageElements)
                    .Add(new Element("Header", "headerFooterType"))
                    .Add(new Element("Footer", "headerFooterType"))
                    .Add(new Element("TextFrame", "textFrameType"))
                    .Add(new Element("Table", "tableType"))
                    .Add(new Element("PageBreak", "emptyType"))
                    .Add(new Element("Graphics", "graphicsType"))
                )
            );

            Types.Add(new ComplexType("insertType")
                .Add(new Attribute("Path", "xs:string"))
                .Add(new Attribute("Resource", "xs:string"))
				.Add(new Attribute("Text", "xs:string"))
                .Add(new AnyAttribute("skip"))
            );

            Types.Add(new ComplexType("sectionType")
                .Add(new Attribute("PageSetup.DifferentFirstPageHeaderFooter", "boolType", documentation: "Defines whether the section has a different first page header &amp; footer"))
                .Add(new Attribute("PageSetup.OddAndEvenPagesHeaderFooter", "boolType", documentation: "Defines whether the odd and even pages of the section have a different header &amp; footer"))
                .Add(new Attribute("PageSetup.HorizontalPageBreak", "boolType", documentation: "Defines whether a page should break horizontally. Currently only tables are supported"))
                .Add(new Attribute("PageSetup.BottomMargin", "unitType"))
                .Add(new Attribute("PageSetup.LeftMargin", "unitType"))
                .Add(new Attribute("PageSetup.RightMargin", "unitType"))
                .Add(new Attribute("PageSetup.TopMargin", "unitType"))
                .Add(new Attribute("PageSetup.HorizontalMargin", "unitType", documentation: "Sets the left &amp; right margins"))
                .Add(new Attribute("PageSetup.VerticalMargin", "unitType", documentation: "Sets the top &amp; bottom margins"))
                .Add(new Attribute("PageSetup.Margin", "unitType", documentation: "Sets all margins"))
                .Add(new Attribute("PageSetup.MirrorMargins", "boolType", documentation: "Defines whether the odd and even pages of the section should change left and right margins"))
                .Add(new Attribute("PageSetup.FooterDistance", "unitType", documentation: "The distance between the footer and the page bottom of the pages in the section"))
                .Add(new Attribute("PageSetup.HeaderDistance", "unitType", documentation: "The distance between the header and the page top of the pages in the section"))
                .Add(new Attribute("PageSetup.Orientation", "orientationType"))
                .Add(new Attribute("PageSetup.PageFormat", "pageFormatType"))
                .Add(new Attribute("PageSetup.PageWidth", "unitType"))
                .Add(new Attribute("PageSetup.PageHeight", "unitType"))
                .Add(new Attribute("PageSetup.SectionStart", "breakTypeType", documentation: "Defines whether the section starts on next, odd or even page"))
                .Add(new Attribute("PageSetup.StartingNumber", "intType", documentation: "The starting number for the first section page"))
                .Add(new Attribute("PageSetup.ContentWidth", "unitType", documentation: "Sets the PageWidth to the value + the left &amp; right margin values"))
                .Add(new Attribute("PageSetup.ContentHeight", "unitType", documentation: "Sets the PageHeight to the value + the top &amp; bottom margin values"))
				.Add(new Attribute("Tag", "xs:string"))
				.Add(styleAttribute)
                .Add(new ChoiceElement(0, int.MaxValue)
                    .Add(LogicalElements)
                    .Add(ParagraphElements)
                    .Add(PointListElements)
                    .Add(new Element("Header", "headerFooterType"))
                    .Add(new Element("Footer", "headerFooterType"))
                    .Add(new Element("TextFrame", "textFrameType"))
                    .Add(ImageElements)
                    .Add(new Element("Table", "tableType"))
                    .Add(new Element("PageBreak", "emptyType"))
                    .Add(new Element("Chart", "chartType"))
                    .Add(new Element("Graphics", "graphicsType"))
                )
            );

            Types.Add(new ComplexType("logicalType")
                .Add(new ChoiceElement(0, int.MaxValue)
                    .Add(LogicalElements)
                    .Add(ParagraphElements)
                    .Add(ParagraphFormattingElements)
                    .Add(ParagraphFieldElements)
                    .Add(HyperlinkElements)
                    .Add(PointListElements)
                    .Add(new Element("Header", "headerFooterType"))
                    .Add(new Element("Footer", "headerFooterType"))
                    .Add(new Element("TextFrame", "textFrameType"))
                    .Add(new Element("Section", "sectionType"))
                    .Add(ImageElements)
                    .Add(GraphicsElements)
                    .Add(new Element("Table", "tableType"))
                    .Add(new Element("PageBreak", "emptyType"))
                    .Add(new Element("Column", "columnType"))
                    .Add(new Element("Row", "rowType"))
                    .Add(new Element("Cell", "cellType"))
					.Add(new Element("Document", "documentType"))
					.Add(new Element("Page", "pageType"))
					.Add(new Element("Area", "areaType"))
					.Add(new Element("SourcePage", "sourcePageType"))
                    .Add(Enumerable.Range(0, 21).Select(x => new Element("C" + x, "cellType")))
                )
            );

            Types.Add(new ComplexType("logicalTestType")
                .ExtendsComplex("logicalType")
                .Add(new Attribute("Test", "xs:string", use: "required"))
            );

            Types.Add(new ComplexType("forEachType")
                .ExtendsComplex("logicalType")
                .Add(new Attribute("Var", "xs:string", use: "required"))
                .Add(new Attribute("In", "xs:string", use: "required"))
            );

            Types.Add(new ComplexType("formattedTextType")
                .ExtendsSimple("xs:string")
                .Add(new Attribute("Font.Bold", "boolType"))
                .Add(new Attribute("Font.Italic", "boolType"))
                .Add(new Attribute("Font.Size", "unitType"))
                .Add(new Attribute("Font.Color", "xs:string"))
                .Add(new Attribute("Font.Name", "xs:string"))
                .Add(new Attribute("Font.Subscript", "boolType"))
                .Add(new Attribute("Font.Superscript", "boolType"))
                .Add(new Attribute("Font.Underline", "underlineType"))
				.Add(styleAttribute)
            );

            Types.Add(new ComplexType("dateFieldType")
                .Add(new Attribute("Format", "xs:string"))
                .Add(styleAttribute)
            );

            Types.Add(new ComplexType("pageFieldType")
                .Add(new Attribute("Format", "xs:string"))
                .Add(styleAttribute)
            );

            Types.Add(new ComplexType("numPagesFieldType")
                .Add(new Attribute("Format", "xs:string"))
                .Add(styleAttribute)
            );

            Types.Add(new ComplexType("paragraphFormatType")
                .Add(new Attribute("Format.Alignment", "paragraphAlignmentType"))
                .Add(new Attribute("Format.FirstLineIndent", "unitType"))
                .Add(new Attribute("Format.KeepTogether", "boolType", documentation: "Indicates whether to keep all the paragraph's lines on the same page"))
                .Add(new Attribute("Format.KeepWithNext", "boolType", documentation: "Indicates whether this and the next paragraph stay on the same page"))
                .Add(new Attribute("Format.LeftIndent", "unitType"))
                .Add(new Attribute("Format.LineSpacing", "unitType", documentation: "The sapce between lines in the paragraph"))
                .Add(new Attribute("Format.LineSpacingRule", "lineSpacingRuleType", documentation: "The rule which is used to define the line spacing"))
                .Add(new Attribute("Format.OutlineLevel", "outlineLevelType"))
                .Add(new Attribute("Format.PageBreakBefore", "boolType", documentation: "Indicates whether a page break is inserted before the paragraph"))
                .Add(new Attribute("Format.RightIndent", "unitType"))
                .Add(new Attribute("Format.SpaceAfter", "unitType", documentation: "The space that's inserted after the paragraph"))
                .Add(new Attribute("Format.SpaceBefore", "unitType", documentation: "The space that's inserted before the paragraph"))
                .Add(new Attribute("Format.WidowControl", "boolType", documentation: "Indicates whether a line from the paragraph stays alone in a page"))
                .Add(FontAttributes.Select(x => new Attribute("Format." + x.Name, x.Type, x.Use, x.Default)))
                .Add(ShadingAttributes.Select(x => new Attribute("Format." + x.Name, x.Type, x.Use, x.Default)))
                .Add(BordersAttributes.Select(x => new Attribute("Format." + x.Name, x.Type, x.Use, x.Default)))
            );
            
            Types.Add(new ComplexType("paragraphType", mixed: true)
                .ExtendsComplex("paragraphFormatType")
                .Add(styleAttribute)
				.Add(new Attribute("Tag", "xs:string"))
				.Add(new ChoiceElement(0, int.MaxValue)
                    .Add(LogicalElements)
                    .Add(HyperlinkElements)
                    .Add(ParagraphFormattingElements)
                    .Add(ParagraphFieldElements)
                    .Add(ImageElements)
					.Add(new Element("Graphics", "graphicsType"))
				)
            );

            Types.Add(new ComplexType("hyperlinkType", mixed: true)
                .Add(FontAttributes)
                .Add(new Attribute("Name", "xs:string"))
                .Add(new Attribute("Type", "hyperlinkTypeType"))
				.Add(new Attribute("Tag", "xs:string"))
				.Add(new ChoiceElement(0, int.MaxValue)
                    .Add(LogicalElements)
                    .Add(ParagraphFormattingElements)
                    .Add(ParagraphFieldElements)
                    .Add(ImageElements)
                )
            );

            Types.Add(new ComplexType("pointListType")
                .ExtendsComplex("paragraphFormatType")
                .Add(new Attribute("Type", "pointListTypeType"))
                .Add(new Attribute("NumberPosition", "unitType"))
				.Add(new Attribute("Tag", "xs:string"))
				.Add(styleAttribute)
                .Add(new ChoiceElement(0, int.MaxValue)
                    .Add(LogicalElements)
                    .Add(PointListElements)
                    .Add(ParagraphElements)
                    .Add(new Element("Graphics", "graphicsType"))
                )
            );

            Types.Add(new ComplexType("shapeType")
                .Add(FillFormatAttributes)
                .Add(LineFormatAttributes)
                .Add(new Attribute("WrapFormat.DistanceBottom", "unitType"))
                .Add(new Attribute("WrapFormat.DistanceLeft", "unitType"))
                .Add(new Attribute("WrapFormat.DistanceRight", "unitType"))
                .Add(new Attribute("WrapFormat.DistanceTop", "unitType"))
                .Add(new Attribute("WrapFormat.Style", "wrapStyleType", documentation: "Specifies how the shape object should be placed between the other elements"))
                .Add(new Attribute("Height", "unitType"))
                .Add(new Attribute("Left", "shapePositionType", documentation: "The position of the left side of the shape"))
                .Add(new Attribute("RelativeHorizontal", "relativeHorizontalType", documentation: "What the shape is placed horizontally in relation to"))
                .Add(new Attribute("RelativeVertical", "relativeHorizontalType", documentation: "What the shape is placed vertically in relation to"))
                .Add(new Attribute("Top", "shapePositionType", documentation: "The position of the top side of the shape"))
                .Add(new Attribute("Width", "unitType"))
				.Add(new Attribute("Tag", "xs:string"))
				.Add(styleAttribute)
            );

            Types.Add(new ComplexType("imageType")
                .ExtendsComplex("shapeType")
                .Add(new Attribute("LockAspectRatio", "boolType"))
                .Add(new Attribute("Name", "xs:string", documentation: "The image file to be added"))
                .Add(new Attribute("Resolution", "doubleType", documentation: "User defined resolution for the image in dots per inch"))
                .Add(new Attribute("ScaleHeight", "doubleType", documentation: "The scale height of the image. If Height is set too, the resulting image height is ScaleHeight * Height"))
                .Add(new Attribute("ScaleWidth", "doubleType", documentation: "The scale width of the image. If Width is set too, the resulting image width is ScaleWidth * Width"))
            );

            Types.Add(new ComplexType("textFrameType")
                .ExtendsComplex("shapeType")
                .Add(new Attribute("MarginBottom", "unitType", documentation: "The margin between the textframe's content and its bottom edge"))
                .Add(new Attribute("MarginLeft", "unitType", documentation: "The margin between the textframe's content and its left edge"))
                .Add(new Attribute("MarginRight", "unitType", documentation: "The margin between the textframe's content and its right edge"))
                .Add(new Attribute("MarginTop", "unitType", documentation: "The margin between the textframe's content and its top edge"))
                .Add(new Attribute("Orientation", "textOrientationType", documentation: "The text orientation for the textframe content"))
                .Add(GetComplexType("paragraphFormatType").Attributes)
                .Add(new ChoiceElement(0, int.MaxValue)
                    .Add(LogicalElements)
                    .Add(ParagraphElements)
                    .Add(PointListElements)
                    .Add(ImageElements)
                    .Add(new Element("Table", "tableType"))
                    .Add(new Element("Chart", "chartType"))
                    .Add(new Element("Graphics", "graphicsType"))
                )
            );

            Types.Add(new ComplexType("headerFooterType")
                .Add(new Attribute("IsEvenPage", "boolType"))
                .Add(new Attribute("IsFirstPage", "boolType"))
                .Add(new Attribute("IsPrimary", "boolType"))
				.Add(new Attribute("Tag", "xs:string"))
				.Add(new ChoiceElement(0, int.MaxValue)
                    .Add(LogicalElements)
                    .Add(ParagraphElements)
                    .Add(PointListElements)
                    .Add(ImageElements)
                    .Add(new Element("Table", "tableType"))
                    .Add(new Element("TextFrame", "textFrameType"))
                )
            );

            #region Tables
            Types.Add(new ComplexType("tableType")
                .ExtendsComplex("paragraphFormatType")
                .Add(new Attribute("BottomPadding", "unitType"))
                .Add(new Attribute("LeftPadding", "unitType"))
                .Add(new Attribute("RightPadding", "unitType"))
                .Add(new Attribute("TopPadding", "unitType"))
                .Add(new Attribute("KeepTogether", "boolType", documentation: "Indicates whether to keep all the table rows on the same page"))
				.Add(new Attribute("Tag", "xs:string"))
				.Add(BordersAttributes)
                .Add(ShadingAttributes)
                .Add(styleAttribute)
                .Add(new ChoiceElement(0, int.MaxValue)
                    .Add(LogicalElements)
                    .Add(new Element("Column", "columnType"))
                    .Add(new Element("Row", "rowType"))
                    .Add(new Element("Graphics", "graphicsType"))
                )
            );

            Types.Add(new ComplexType("columnType")
                .ExtendsComplex("paragraphFormatType")
                .Add(new Attribute("Heading", "boolType"))
                .Add(new Attribute("KeepWith", "intType", documentation: "The number of columns that should be kept together with current column in case of a page break"))
                .Add(new Attribute("LeftPadding", "unitType"))
                .Add(new Attribute("RightPadding", "unitType"))
                .Add(new Attribute("Width", "unitType"))
				.Add(new Attribute("Tag", "xs:string"))
				.Add(BordersAttributes)
                .Add(ShadingAttributes)
                .Add(styleAttribute)
            );

            Types.Add(new ComplexType("rowType")
                .ExtendsComplex("paragraphFormatType")
                .Add(new Attribute("BottomPadding", "unitType"))
                .Add(new Attribute("Heading", "boolType", documentation: "In the case of a page break part way through the table, the header row(s) are displayed at the top of the continuation on the next page"))
                .Add(new Attribute("Height", "unitType"))
                .Add(new Attribute("HeightRule", "rowHeightRuleType"))
                .Add(new Attribute("KeepWith", "intType", documentation: "The number of rows that should be kept together with the current row in case of a page break"))
                .Add(new Attribute("TopPadding", "unitType"))
                .Add(new Attribute("VerticalAlignment", "verticalAlignmentType", documentation: "The vertical alignment of all cells in the row"))
				.Add(new Attribute("Tag", "xs:string"))
				.Add(BordersAttributes)
                .Add(ShadingAttributes)
                .Add(styleAttribute)
                .Add(Enumerable.Range(0, 21).Select(x => new Attribute("C" + x, "xs:string")))
                .Add(new ChoiceElement(0, int.MaxValue)
                    .Add(LogicalElements)
                    .Add(new Element("Cell", "cellType"))
                    .Add(Enumerable.Range(0, 21).Select(x => new Element("C" + x, "cellType")))
                    .Add(new Element("Graphics", "graphicsType"))
                )
            );

            Types.Add(new ComplexType("cellType", mixed: true)
                .ExtendsComplex("paragraphFormatType")
                .Add(BordersAttributes)
                .Add(ShadingAttributes)
                .Add(new Attribute("MergeDown", "intType", documentation: "The number of cells below to be merged into this one"))
                .Add(new Attribute("MergeRight", "intType", documentation: "The number of cells to the right to be merged into this one"))
                .Add(styleAttribute)
                .Add(new Attribute("Index", "intType", documentation: "The index of the column that the cell belongs to"))
                .Add(new Attribute("VerticalAlignment", "verticalAlignmentType"))
				.Add(new Attribute("Tag", "xs:string"))
				.Add(new ChoiceElement(0, int.MaxValue)
                    .Add(LogicalElements)
                    .Add(ParagraphElements)
                    .Add(PointListElements)
                    .Add(new Element("Table", "tableType"))
                    .Add(new Element("TextFrame", "textFrameType"))
                    .Add(ImageElements)
                    .Add(new Element("Chart", "chartType"))
                    .Add(new Element("Graphics", "graphicsType"))
                 )
            );
            #endregion Tables

            #region Charts
            Types.Add(new SimpleType("tickMarkTypeType")
                .RestrictByEnumeration("xs:string", "Cross", "Inside", "None", "Outside")
            );

            Types.Add(new SimpleType("chartTypeType")
                .RestrictByEnumeration("xs:string", "Area2D", "Bar2D", "BarStacked2D", "Column2D", "ColumnStacked2D", "Line", "Pie2D", "PieExploded2D")
            );

            Types.Add(new SimpleType("dataLabelPositionType")
                .RestrictByEnumeration("xs:string", "Center", "InsideBase", "InsideEnd", "OutsideEnd")
            );

            Types.Add(new SimpleType("dataLabelTypeType")
                .RestrictByEnumeration("xs:string", "None", "Percent", "Value")
            );

            Types.Add(new SimpleType("blankTypeType")
                .RestrictByEnumeration("xs:string", "Interpolated", "NotPlotted", "Zero")
            );

            Types.Add(new ComplexType("axisType")
                .Add(new Attribute("HasMajorGridlines", "boolType"))
                .Add(new Attribute("HasMinorGridlines", "boolType"))
                .Add(LineFormatAttributes)
                .Add(LineFormatAttributes.Select(x => new Attribute("MajorGridlines." + x.Name, x.Type, x.Use, x.Default)))
                .Add(new Attribute("MajorTick", "doubleType"))
                .Add(new Attribute("MajorTickMark", "tickMarkTypeType"))
                .Add(new Attribute("MaximumScale", "doubleType"))
                .Add(new Attribute("MinimumScale", "doubleType"))
                .Add(LineFormatAttributes.Select(x => new Attribute("MinorGridlines." + x.Name, x.Type, x.Use, x.Default)))
                .Add(new Attribute("MinorTick", "doubleType"))
                .Add(new Attribute("MinorTickMark", "tickMarkTypeType"))
                .Add(FontAttributes.Select(x => new Attribute("TickLabels." + x.Name, x.Type, x.Use, x.Default, x.Documentation)))
                .Add(new Attribute("TickLabels.Format", "xs:string"))
                .Add(new Attribute("Title.Alignment", "horizontalAlignmentType"))
                .Add(new Attribute("Title.Caption", "xs:string"))
                .Add(FontAttributes.Select(x => new Attribute("Title." + x.Name, x.Type, x.Use, x.Default, x.Documentation)))
                .Add(new Attribute("Title.Orientation", "unitType"))
                .Add(new Attribute("Title.VerticalAlignment", "verticalAlignmentType"))
				.Add(new Attribute("Tag", "xs:string"))
			);

            Types.Add(new ComplexType("plotAreaType")
                .Add(new Attribute("BottomPadding", "unitType"))
                .Add(FillFormatAttributes)
                .Add(new Attribute("LeftPadding", "unitType"))
                .Add(LineFormatAttributes)
                .Add(new Attribute("RightPadding", "unitType"))
                .Add(new Attribute("TopPadding", "unitType"))
				.Add(new Attribute("Tag", "xs:string"))
			);

            Types.Add(new ComplexType("textAreaType")
                .ExtendsComplex("paragraphFormatType")
                .Add(new Attribute("BottomPadding", "unitType"))
                .Add(FillFormatAttributes)
                .Add(new Attribute("Height", "unitType"))
                .Add(new Attribute("LeftPadding", "unitType"))
                .Add(LineFormatAttributes)
                .Add(new Attribute("RightPadding", "unitType"))
                .Add(new Attribute("TopPadding", "unitType"))
                .Add(new Attribute("VerticalAlignment", "verticalAlignmentType"))
                .Add(new Attribute("Width", "unitType"))
				.Add(new Attribute("Tag", "xs:string"))
				.Add(new ChoiceElement(0, int.MaxValue)
                    .Add(LogicalElements)
                    .Add(new Element("Legend", "legendType"))
                )
            );

            Types.Add(new ComplexType("legendType")
                .ExtendsComplex("paragraphFormatType")
				.Add(new Attribute("Tag", "xs:string"))
				.Add(LineFormatAttributes)
            );

            Types.Add(new ComplexType("seriesType")
                .Add(new Attribute("Name", "xs:string"))
                .Add(new Attribute("Values", "xs:string", use: "required"))
                .Add(new Attribute("ChartType", "chartTypeType"))
                .Add(FillFormatAttributes)
                .Add(new Attribute("HasDataLabel", "boolType"))
                .Add(LineFormatAttributes)
                .Add(new Attribute("MarkerBackgroundColor", "colorType"))
                .Add(new Attribute("MarkerForegroundColor", "colorType"))
                .Add(new Attribute("MarkerSize", "unitType"))
                .Add(new Attribute("MarkerStyle", "xs:string"))
                .Add(FontAttributes.Select(x => new Attribute("DataLabel." + x.Name, x.Type, x.Use, x.Default, x.Documentation)))
                .Add(new Attribute("DataLabel.Format", "xs:string"))
                .Add(new Attribute("DataLabel.Position", "dataLabelPositionType"))
                .Add(new Attribute("DataLabel.Type", "dataLabelTypeType"))
				.Add(new Attribute("Tag", "xs:string"))
			);

            Types.Add(new ComplexType("dataLabelType")
                .Add(FontAttributes)
                .Add(new Attribute("Format", "xs:string"))
                .Add(new Attribute("Position", "dataLabelPositionType"))
                .Add(new Attribute("Type", "dataLabelTypeType"))
				.Add(new Attribute("Tag", "xs:string"))
			);

            Types.Add(new ComplexType("xSeriesType")
                .Add(new Attribute("Values", "xs:string", use: "required"))
            );

            Types.Add(new ComplexType("chartType")
                .ExtendsComplex("shapeType")
                .Add(new Attribute("DisplayBlanksAs", "blankTypeType"))
                .Add(new Attribute("Type", "chartTypeType"))
                .Add(new Attribute("Format.Alignment", "paragraphAlignmentType"))
                .Add(new Attribute("Format.FirstLineIndent", "unitType"))
                .Add(new Attribute("Format.KeepTogether", "boolType", documentation: "Indicates whether to keep all the paragraph's lines on the same page"))
                .Add(new Attribute("Format.KeepWithNext", "boolType", documentation: "Indicates whether this and the next paragraph stay on the same page"))
                .Add(new Attribute("Format.LeftIndent", "unitType"))
                .Add(new Attribute("Format.LineSpacing", "unitType", documentation: "The sapce between lines in the paragraph"))
                .Add(new Attribute("Format.LineSpacingRule", "lineSpacingRuleType", documentation: "The rule which is used to define the line spacing"))
                .Add(new Attribute("Format.OutlineLevel", "outlineLevelType"))
                .Add(new Attribute("Format.PageBreakBefore", "boolType", documentation: "Indicates whether a page break is inserted before the paragraph"))
                .Add(new Attribute("Format.RightIndent", "unitType"))
                .Add(new Attribute("Format.SpaceAfter", "unitType", documentation: "The space that's inserted after the paragraph"))
                .Add(new Attribute("Format.SpaceBefore", "unitType", documentation: "The space that's inserted before the paragraph"))
                .Add(new Attribute("Format.WidowControl", "boolType", documentation: "Indicates whether a line from the paragraph stays alone in a page"))
                .Add(FontAttributes.Select(x => new Attribute("Format." + x.Name, x.Type, x.Use, x.Default)))
                .Add(ShadingAttributes.Select(x => new Attribute("Format." + x.Name, x.Type, x.Use, x.Default)))
                .Add(BordersAttributes.Select(x => new Attribute("Format." + x.Name, x.Type, x.Use, x.Default)))
                .Add(new Attribute("PivotChart", "boolType"))
				.Add(new Attribute("Tag", "xs:string"))
				.Add(new ChoiceElement(0, int.MaxValue)
                    .Add(LogicalElements)
                    .Add(new Element("TopArea", "textAreaType"))
                    .Add(new Element("BottomArea", "textAreaType"))
                    .Add(new Element("FooterArea", "textAreaType"))
                    .Add(new Element("HeaderArea", "textAreaType"))
                    .Add(new Element("LeftArea", "textAreaType"))
                    .Add(new Element("RightArea", "textAreaType"))
                    .Add(new Element("PlotArea", "plotAreaType"))
                    .Add(new Element("Series", "seriesType"))
                    .Add(new Element("XSeries", "xSeriesType"))
                    .Add(new Element("XAxis", "axisType"))
                    .Add(new Element("YAxis", "axisType"))
                    .Add(new Element("ZAxis", "axisType"))
                    .Add(new Element("DataLabel", "dataLabelType"))
                )
            );
            #endregion Charts

            #region Graphics
            Types.Add(new ComplexType("graphicsType")
                .Add(new Attribute("Page", "intType"))
                .Add(new ChoiceElement(0, int.MaxValue)
                    .Add(LogicalElements)
                    .Add(GraphicsElements)
                )
            );

            Types.Add(new ComplexType("graphicsLineType")
                .Add(new Attribute("Page", "intType"))
                .Add(PenAttributes)
                .Add(new ChoiceElement(0, int.MaxValue)
                    .Add(new Element("Point", "graphicsPointType"))
                    .Add(LogicalElements)
                )
            );

            Types.Add(new ComplexType("graphicsBezierType")
                .Add(new Attribute("Page", "intType"))
                .Add(PenAttributes)
                .Add(new ChoiceElement(0, int.MaxValue)
                    .Add(new Element("Point", "graphicsPointType"))
                    .Add(LogicalElements)
                )
            );

			Types.Add(new ComplexType("textAnnotationType")
				.Add(new Attribute("Page", "intType"))
				.Add(new Attribute("Title", "xs:string"))
				.Add(new Attribute("Subject", "xs:string"))
				.Add(new Attribute("Contents", "xs:string"))
				.Add(new Attribute("Icon", "textAnnotationIconType"))
				.Add(new Attribute("Color", "colorType"))
				.Add(new Attribute("Opacity", "doubleType"))
				.Add(new Attribute("Open", "boolType"))
				.Add(new Attribute("Left", "unitType"))
				.Add(new Attribute("Top", "unitType"))
				.Add(new Attribute("Width", "unitType"))
				.Add(new Attribute("Right", "unitType"))
				.Add(new Attribute("Height", "unitType"))
				.Add(new Attribute("Bottom", "unitType"))
				.Add(new Attribute("Area", "scriptType"))
			);

            Types.Add(new ComplexType("graphicsPointType")
                .Add(new Attribute("X", "unitType"))
                .Add(new Attribute("Y", "unitType"))
            );

            Types.Add(new ComplexType("graphicsRectType")
                .Add(new Attribute("Page", "intType"))
                .Add(PenAttributes)
                .Add(new Attribute("Left", "unitType"))
                .Add(new Attribute("Width", "unitType"))
                .Add(new Attribute("Right", "unitType"))
                .Add(new Attribute("Top", "unitType"))
                .Add(new Attribute("Height", "unitType"))
                .Add(new Attribute("Bottom", "unitType"))
                .Add(new Attribute("Corner", "unitType"))
                .Add(new Attribute("CornerX", "unitType"))
                .Add(new Attribute("CornerY", "unitType"))
                .Add(new Attribute("Area", "scriptType"))
				.Add(new Attribute("Brush.Color", "colorType"))
				.Add(new Attribute("Brush.Color2", "colorType"))
				.Add(new Attribute("Brush.GradientMode", "graphicsGradientMode"))
            );

			Types.Add(new ComplexType("graphicsStringType", mixed: true)
				.Add(new Attribute("Page", "intType"))
				.Add(new Attribute("Brush.Color", "colorType"))
				.Add(new Attribute("Brush.Color2", "colorType"))
				.Add(new Attribute("Brush.GradientMode", "graphicsGradientMode"))
				.Add(new Attribute("Font.FamilyName", "xs:string"))
				.Add(new Attribute("Font.EmSize", "doubleType"))
				.Add(new Attribute("Font.Style", "graphicsFontStyleType"))
				.Add(new Attribute("Font.Options", "graphicsFontOptionsType"))
				.Add(new Attribute("Point.X", "unitType"))
				.Add(new Attribute("Point.Y", "unitType"))
				.Add(new Attribute("Area", "scriptType"))
				.Add(new Attribute("Value", "xs:string"))
			);

            Types.Add(new SimpleType("graphicsDashStyleType")
                .RestrictByEnumeration("xs:string", "Custom", "Dash", "DashDot", "DashDotDot", "Dot", "Solid")
            );

            Types.Add(new SimpleType("graphicsLineCapType")
                .RestrictByEnumeration("xs:string", "Flat", "Round", "Square")
            );

            Types.Add(new SimpleType("graphicsLineJoinType")
                .RestrictByEnumeration("xs:string", "Bevel", "Miter", "Round")
            );

			Types.Add(new SimpleType("graphicsGradientMode")
				.RestrictByEnumeration("xs:string", "BackwardDiagonal", "ForwardDiagonal", "Horizontal", "Vertical")
			);

			Types.Add(new SimpleType("graphicsFontStyleType")
				.RestrictByEnumeration("xs:string", "Regular", "Bold", "Italic", "BoldItalic", "Underline", "Strikeout")
			);

			Types.Add(new SimpleType("graphicsFontOptionsType")
				.RestrictByEnumeration("xs:string", "UnicodeDefault", "WinAnsiDefault")
			);
            #endregion Graphics

            Add(new Element("Document", "documentType"));
			Add(new Element("Documents", "documentsType"));

            return this;
        }

    }
}
