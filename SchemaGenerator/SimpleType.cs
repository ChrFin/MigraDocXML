﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemaGenerator
{
    public class SimpleType : SchemaType
    {

        public ExtensionBase Extension { get; set; }

        public SimpleType(string name)
            : base(name)
        {
        }

        public SimpleType RestrictByPattern(string baseType, string pattern)
        {
            Extension = new PatternRestriction(baseType, pattern);
            return this;
        }

        public SimpleType RestrictByEnumeration(string baseType, params string[] values)
        {
            Extension = new EnumerableRestriction(baseType, values);
            return this;
        }

        public SimpleType RestrictByUnion(params string[] memberTypes)
        {
            Extension = new Union(memberTypes);
            return this;
        }


        public abstract class ExtensionBase
        {
            public string BaseType { get; private set; }

            public ExtensionBase(string baseType)
            {
                BaseType = baseType;
            }
        }

        public class PatternRestriction : ExtensionBase
        {
            public string Pattern { get; private set; }

            public PatternRestriction(string baseType, string pattern)
                : base(baseType)
            {
                Pattern = pattern;
            }
        }

        public class EnumerableRestriction : ExtensionBase
        {
            public List<string> Values { get; private set; }

            public EnumerableRestriction(string baseType, params string[] values)
                : base(baseType)
            {
                Values = values.ToList();
            }
        }

        public class Union : ExtensionBase
        {
            public List<string> MemberTypes { get; private set; }

            public Union(params string[] memberTypes)
                : base("")
            {
                MemberTypes = memberTypes.ToList();
            }
        }

    }
}
